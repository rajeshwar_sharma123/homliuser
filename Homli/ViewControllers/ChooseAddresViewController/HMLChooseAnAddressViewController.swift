//
//  HMLChooseAnAddressViewController.swift
//  Homli
//
//  Created by daffolapmac on 11/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLChooseAnAddressViewController: HMLBaseViewController, GMSMapViewDelegate, CLLocationManagerDelegate, HMLSearchAddressViewDelegate {

	@IBOutlet var mapView: GMSMapView!
	@IBOutlet var markerImgView: UIImageView!
	@IBOutlet weak var icnSearchImgView: UIImageView!
	@IBOutlet var searchView: UIView!
	@IBOutlet var lblAddress: UILabel!
	@IBOutlet var btnCurrentLocation: UIButton!
	@IBOutlet var btnUseThisAddress: UIButton!
	var formatedAddressDic: NSDictionary!
	var locationManager = CLLocationManager()
	var myLocation: CLLocation!
	var flag: Bool!
	var shouldPopToAddressListingVC: Bool! = false
	// MARK:LifeCycle method
	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
		btnUseThisAddress.enabled = false
		self.mapView.bringSubviewToFront(markerImgView)
		self.mapView.bringSubviewToFront(btnCurrentLocation)
		flag = true
		self.title = "Choose An Address"
		setBackButton()
		// delhi 28.6139° N, 77.2090° E
		mapView.delegate = self
		locationManager.requestWhenInUseAuthorization()
		locationManager.delegate = self
		locationManager.startUpdatingLocation()
		markerImgView.setImageColorFromHexaCode(k_Default_Orange_Color)
		icnSearchImgView.setImageColorFromHexaCode(k_Default_Orange_Color)
		searchView.layer.cornerRadius = 2
		searchView.clipsToBounds = true
		btnCurrentLocation.layer.cornerRadius = 2
		btnCurrentLocation.clipsToBounds = true
		btnUseThisAddress.layer.cornerRadius = 2
		btnUseThisAddress.clipsToBounds = true

	}
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController!.navigationBarHidden = false

	}

	// MARK:- GMSMapViewDelegate Method

	func mapView(mapView: GMSMapView, didChangeCameraPosition position: GMSCameraPosition)
	{
		// print("start")

	}
	func mapView(mapView: GMSMapView, idleAtCameraPosition position: GMSCameraPosition) {
		let latitude: Double = mapView.camera.target.latitude
		let longitude: Double = mapView.camera.target.longitude
		getAddressFromLatLon(latitude, longitude: longitude)
	}
	func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
		if status == CLAuthorizationStatus.AuthorizedWhenInUse {
			mapView.myLocationEnabled = false
		}
	}
	func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
	{
		myLocation = locationManager.location!
		if flag == true {

			self.currentLocationButtonAction(btnCurrentLocation)
			flag = false
		}

	}

//    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
//        if !didFindMyLocation {
//            let myLocation: CLLocation = change[NSKeyValueChangeNewKey] as CLLocation
//            viewMap.camera = GMSCameraPosition.cameraWithTarget(myLocation.coordinate, zoom: 10.0)
//            viewMap.settings.myLocationButton = true
//
//            didFindMyLocation = true
//        }
//    }
	// MARK: This method get the Address on the basis of Latitude and longitude
	func getAddress(latitude: Double, longitude: Double)
	{

		let geoCoder = CLGeocoder()
		let location = CLLocation(latitude: latitude, longitude: longitude)

		geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in

			// Place details
			var placeMark: CLPlacemark!

			placeMark = placemarks?[0]

			if (placeMark != nil)
			{
				// Address dictionary
				print(placeMark.addressDictionary)

				let arr: NSArray = placeMark.addressDictionary!["FormattedAddressLines"] as! NSArray
				let address: String = arr.componentsJoinedByString(",")
				self.lblAddress.text = address
				// Location name
				if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
					print(locationName)
				}

				// Street address
				if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
					print(street)
				}

				// City
				if let city = placeMark.addressDictionary!["City"] as? NSString {
					print(city)
				}

				// Zip code
				if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
					print(zip)
				}

				// Country
				if let country = placeMark.addressDictionary!["Country"] as? NSString {
					print(country)
				}

			}

		})

	}

	func getAddressFromLatLon(latitude: Double, longitude: Double)
	{
		let urlStr: String = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + String(format: "%f", latitude) + "," + String(format: "%f", longitude)
		HMLAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in

			print(responseData)
			let arr: NSArray = responseData["results"] as! NSArray
			if (arr.count > 0)
			{
				let formatedAddress: String = arr .firstObject!["formatted_address"] as! String
				self.formatedAddressDic = arr .firstObject as! NSDictionary
				self.lblAddress.text = formatedAddress
				self.btnUseThisAddress.enabled = true
			}

		}) { (errorMessage) -> () in

		}

	}
	// MARK:- Button Action

	@IBAction func searchViewTapAction(sender: AnyObject) {

		let searchAddressView = NSBundle.mainBundle().loadNibNamed("HMLSearchAddressView", owner: self, options: nil)[0] as! HMLSearchAddressView
		searchAddressView.delegate = self
		searchAddressView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
		self.view.addSubview(searchAddressView)

	}
	func holdAddressComponentTemperory() {
		let addressComponent = HMLAddressComponentModel.sharedInstance()
		addressComponent.tmpAddressComponent = HMLAddressComponentModel()
		addressComponent.tmpAddressComponent!.street_number = addressComponent.street_number
		addressComponent.tmpAddressComponent!.route = addressComponent.route
		addressComponent.tmpAddressComponent!.sublocality_level_3 = addressComponent.sublocality_level_3
		addressComponent.tmpAddressComponent!.sublocality_level_2 = addressComponent.sublocality_level_2
		addressComponent.tmpAddressComponent!.sublocality_level_1 = addressComponent.sublocality_level_1
		addressComponent.tmpAddressComponent!.locality = addressComponent.locality
		addressComponent.tmpAddressComponent!.administrative_area_level_2 = addressComponent.administrative_area_level_2
		addressComponent.tmpAddressComponent!.administrative_area_level_1 = addressComponent.administrative_area_level_1
		addressComponent.tmpAddressComponent!.country_long_name = addressComponent.country_long_name
		addressComponent.tmpAddressComponent!.country_short_name = addressComponent.country_short_name
		addressComponent.tmpAddressComponent!.postal_code = addressComponent.postal_code
		addressComponent.tmpAddressComponent!.latitude = addressComponent.latitude
		addressComponent.tmpAddressComponent!.longitude = addressComponent.longitude
		// tmpDeliveryAddrss = deliveryAddrss.mutableCopy() as! HMLDeliveryAddressModel

	}

	@IBAction func useThisAddressButtonAction(sender: AnyObject) {
		if formatedAddressDic.count == 0 {
			return
		}
		let addressComponent = HMLAddressComponentModel.sharedInstance()

		if !(addressComponent.latitude.isEmpty && addressComponent.longitude.isEmpty) {

			holdAddressComponentTemperory()
		}

		addressComponent.bindDataWithModel(self.formatedAddressDic)

		let addressView: HMLConfirmDeliveryAddressView = self.view.loadFromNibNamed("HMLConfirmDeliveryAddressView") as! HMLConfirmDeliveryAddressView
		addressView.initDefaultValue(self.formatedAddressDic)
		addressView.frame = self.view.bounds
		addressView.shouldPopToAddressListingVC = self.shouldPopToAddressListingVC

		addressView.transparentBackground()
		self.view.addSubview(addressView)

	}

	@IBAction func currentLocationButtonAction(sender: AnyObject) {

		let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude, zoom: 13)
		self.mapView.animateToCameraPosition(camera)

	}

	// MARK:- HMLSearchAddressViewDelegate Method

	func getSearchedPlace(place: GMSPlace) {
		let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 13)
		self.mapView.animateToCameraPosition(camera)

	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	/*
	 // MARK: - Navigation

	 // In a storyboard-based application, you will often want to do a little preparation before navigation
	 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	 // Get the new view controller using segue.destinationViewController.
	 // Pass the selected object to the new view controller.
	 }
	 */

}
