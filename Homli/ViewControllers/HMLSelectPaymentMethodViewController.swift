//
//  HMLSelectPaymentMethodViewController.swift
//  Homli
//
//  Created by Jenkins on 4/13/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLSelectPaymentMethodViewController: HMLBaseViewController {
  var paymentMethodArray=["Cash On Delivery(COD)", "Net Banking/Card/Wallet"]
  var selectedIndex:Int?
  @IBOutlet weak var tableView: UITableView!
  override func viewDidLoad() {
    super.viewDidLoad()
    settable()
    setBackButton()
    self.title = "SELECT PAYMENT METHODS"
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}
//MARK: Extension for the UITableViewDelegate,UITableViewDataSource
extension HMLSelectPaymentMethodViewController:  UITableViewDelegate, UITableViewDataSource
{
  func settable()
  {
    tableView.dataSource = self
    tableView.delegate = self

  }
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }

  //MARK: Table View return number of rows
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return paymentMethodArray.count

  }

  //MARK: Table View Data
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
  {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! HMLPaymentMethodTableViewCell
    cell.detailArrayLabel.text=paymentMethodArray[indexPath.row]
   
    if HMLPlaceOrderModel.sharedInstance().selectedPaymentModeIndex == nil{
        cell.radioImg.image = UIImage(named:"ic_option_unselected")
    }
    else{
        if indexPath.row == HMLPlaceOrderModel.sharedInstance().selectedPaymentModeIndex{
            cell.radioImg.image = UIImage(named:"ic_option_selected")
        }
        else {
            cell.radioImg.image = UIImage(named:"ic_option_unselected")
        }
    }
    
    
    return cell
  }

  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    selectedIndex = indexPath.row
    HMLPlaceOrderModel.sharedInstance().selectedPaymentModeIndex = selectedIndex
    if (selectedIndex == 1)
    {
       HMLPlaceOrderModel.sharedInstance().paymentMethod = "\(PaymentMethod.ONLINE)"
    }
    else {
      HMLPlaceOrderModel.sharedInstance().paymentMethod = "\(PaymentMethod.COD)"
    }
    self.navigationController?.popViewControllerAnimated(false)
  }

  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 50
  }

  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    self.tableView.reloadData()
  }


}