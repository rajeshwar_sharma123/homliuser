//
//  HMLAvailableDeliverySlotViewController.swift
//  Homli
//
//  Created by Jenkins on 4/13/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLAvailableDeliverySlotViewController: HMLBaseViewController {

  var deliverySlotArrayTo=[String]()
  var deliverySlotArrayFrom=[String]()
  @IBOutlet weak var tableView: UITableView!
  override func viewDidLoad() {
    super.viewDidLoad()
    settable()
    setBackButton()
    self.title = "SELECT DELIVERY SLOT"
    //getDeliveryDetailFromServer()
  }


  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}

//MARK:Extension for UITableViewDelegate
extension HMLAvailableDeliverySlotViewController:  UITableViewDelegate, UITableViewDataSource
{

  func settable()
  {
    tableView.dataSource = self
    tableView.delegate = self

  }
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }

  //MARK: Table View return number of rows
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if  self.deliverySlotArrayTo.count == 0
    {
      return 0
    }
    else
    {
      return self.deliverySlotArrayTo.count
    }
  }

  //MARK: Table View Data
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
  {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! HMLDeliverySlotTableViewCell
    if deliverySlotArrayTo.count == 0
    {
      cell.deliveryReportLabel.text = "detail"

    }
    else
    {
      cell.deliveryReportLabel.text = "\(self.deliverySlotArrayTo[indexPath.row])  - \(self.deliverySlotArrayFrom[indexPath.row])"
    }

    if (HMLPlaceOrderModel.sharedInstance().selectedSlotIndex == indexPath.row) {

      cell.radioImg.image = UIImage(named:"ic_option_selected")

    } else {
      cell.radioImg.image = UIImage(named:"ic_option_unselected")


    }
    return cell
  }

  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    HMLPlaceOrderModel.sharedInstance().selectedSlotIndex = indexPath.row
    HMLPlaceOrderModel.sharedInstance().deliverySlotStartTime = "\(self.deliverySlotArrayTo[indexPath.row])"
    HMLPlaceOrderModel.sharedInstance().deliverySlotEndTime = "\(self.deliverySlotArrayFrom[indexPath.row])"
    HMLPlaceOrderModel.sharedInstance().deliverySlot = "\(self.deliverySlotArrayTo[indexPath.row]) - \(self.deliverySlotArrayFrom[indexPath.row])"
    self.navigationController?.popViewControllerAnimated(false)
    //tableView.reloadData()
  }

  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 50

  }
    
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    self.tableView.reloadData()
  }
}

