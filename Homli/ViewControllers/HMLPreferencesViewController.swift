//
//  HMLPreferencesViewController.swift
//  Homli
//
//  Created by Daffolap-21 on 14/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLPreferencesViewController: UIViewController {

	@IBOutlet weak var vegButton: UIButton!

	@IBOutlet weak var nonVegButton: UIButton!
	// MARK:LifeCycle method
	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = "PREFERENCES"
		let backButton = UIButton(type: UIButtonType.Custom)
		backButton.addTarget(self, action: #selector(HMLPreferencesViewController.didTapBackButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		// backButton.setTitle("TITLE", forState: UIControlState.Normal)
		backButton.setImage(UIImage(named: "ic_action_close"), forState: UIControlState.Normal)
		backButton.sizeToFit()

		let backButtonItem = UIBarButtonItem(customView: backButton)
		self.navigationItem.leftBarButtonItem = backButtonItem

		let rightButton = UIButton(type: UIButtonType.Custom)
		rightButton.addTarget(self, action: #selector(HMLPreferencesViewController.didTapDoneButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		// backButton.setTitle("TITLE", forState: UIControlState.Normal)
		rightButton.setImage(UIImage(named: "ic_done"), forState: UIControlState.Normal)
		rightButton.sizeToFit()
		let rightButtonItem = UIBarButtonItem(customView: rightButton)
		self.navigationItem.rightBarButtonItem = rightButtonItem

		// Do any additional setup after loading the view.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	/*
	 // MARK: - Navigation

	 // In a storyboard-based application, you will often want to do a little preparation before navigation
	 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	 // Get the new view controller using segue.destinationViewController.
	 // Pass the selected object to the new view controller.
	 }
	 */

	@IBAction func didTapVegButton(sender: AnyObject) {
		self.vegButton.selected = !self.vegButton.selected

	}

	@IBAction func didTapNoVegButton(sender: AnyObject) {

		self.nonVegButton.selected = !self.nonVegButton.selected

	}

	func didTapBackButton(sender: AnyObject)
	{
		self.navigationController?.popViewControllerAnimated(true)
	}

	func didTapDoneButton(sender: AnyObject)
	{
		print("right button tapped")
		self.navigationController?.popViewControllerAnimated(true)
	}

}
