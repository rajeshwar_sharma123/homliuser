//
//  HMLJoinUsViewController.swift
//  Homli
//
//  Created by daffolapmac on 08/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import MFSideMenu
class HMLJoinUsViewController: UIViewController {

	@IBOutlet weak var btnSignIn: UIButton!
	@IBOutlet weak var tnRegister: UIButton!
	@IBOutlet var lblJoinUs: UILabel!
	//MARK:method LifeCycle
    override func viewDidLoad() {
		super.viewDidLoad()
		checkUserSignUpFlow()
		// Do any additional setup after loading the view.
		self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
	}
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController!.navigationBarHidden = true
	}
	@IBAction func signInButtonAction(sender: AnyObject) {
		self.pushViewController(withIdentifireName: "HMLSignInViewController", inNavigationController: self.navigationController!)
		return
	}

	@IBAction func registerButtonAction(sender: AnyObject) {
		self.pushViewController(withIdentifireName: "HMLRegisterViewController", inNavigationController: self.navigationController!)

	}
	@IBAction func helpButtonAction(sender: AnyObject) {
		let view = NSBundle.mainBundle().loadNibNamed("HMLJoinUsHelpView", owner: self, options: nil)[0] as! UIView
		view.frame = self.view.frame
		self.view.addSubview(view)
	}
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	/*
	 // MARK: - Navigation

	 // In a storyboard-based application, you will often want to do a little preparation before navigation
	 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	 // Get the new view controller using segue.destinationViewController.
	 // Pass the selected object to the new view controller.
	 }
	 */

	func navigateToOTPVerificationScreen()
	{
		let verificationViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMLVerificationViewController") as? HMLVerificationViewController
		self.navigationController?.pushViewController(verificationViewController!, animated: false)
	}
	func navigateToAddressDetailScreen()
	{
		let addressDetailViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMLAddressDetailViewController") as? HMLAddressDetailViewController
		self.navigationController?.pushViewController(addressDetailViewController!, animated: false)
	}
	/**
	 This method check the UserSignUp Flow
	 */
	func checkUserSignUpFlow()
	{
		if HMLUtlities.isUserInSignUpProcess()
		{
			if HMLUtlities.isUserOTPVerified() {

				if HMLUtlities.isUserAddressDetailFilled() {

				}
				else {
					navigateToAddressDetailScreen()
				}

			}
			else
			{
				navigateToOTPVerificationScreen()
			}

		}

	}
}
