//
//  HMLReferralViewController.swift
//  Homli
//
//  Created by Vishwas Singh on 3/18/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLReferralViewController: HMLBaseViewController {
    
    @IBOutlet weak var referralCodeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        self.title = "REFERRAL"
        
        let signInUserDic = HMLUtlities.getSignInUserDetail()
        self.referralCodeLabel.text =  "\(signInUserDic["refferalCode"] as! String)"
        setSideMenuPanModeNone()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnInvite(sender: AnyObject) {
        print("reffral button")
        let signInUserDic = HMLUtlities.getSignInUserDetail()
        let activityViewController = UIActivityViewController(
            activityItems: [signInUserDic["refferalCode"]!],
            applicationActivities: nil)
        
        presentViewController(activityViewController, animated: true, completion: nil)
    }
}
