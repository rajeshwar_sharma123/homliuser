

import UIKit

class HMLAddressDetailViewController: HMLBaseViewController {
    
    //======textfield======
    @IBOutlet var txtFieldLandmark: UITextField!
    @IBOutlet var txtFieldPincode: UITextField!
    @IBOutlet var txtFieldAddressLine2: UITextField!
    @IBOutlet var txtFieldAddressLine1: UITextField!
    @IBOutlet var btnVeg: UIButton!
    @IBOutlet var btnNonVeg: UIButton!
    
    @IBOutlet weak var btnAddLocation: UIButton!
    var signUpUserDic : NSMutableDictionary?
    let  deliveryAddrss = HMLDeliveryAddressModel.sharedInstance()
    let addressComponent = HMLAddressComponentModel.sharedInstance()
    
    //====didload function====
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        self.title = "ADDRESS DETAIL"
        signUpUserDic = HMLUtlities.getSignUpUserDetail()
        HMLUtlities.setUserFromAddressScreen(true)
        btnAddLocation.setImageColorFromHexaCodeForNormalState(k_Default_Orange_Color)
        addressComponent.isFromConfirmYourAddressScreen = false
    }
   
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = false
        NSNotificationCenter.defaultCenter().addObserver( self,selector: #selector(HMLAddressDetailViewController.didReceiveDeliveryAddressNotification(_:)),name: "DeliveryAddressNotification", object: nil)
    }
    
    deinit
    {
     NSNotificationCenter.defaultCenter().removeObserver(self, name: "DeliveryAddressNotification", object: nil)
    }
    
    func didReceiveDeliveryAddressNotification(notification: NSNotification){
        
        if notification.name == "DeliveryAddressNotification" {
            let  deliveryAddrss = HMLDeliveryAddressModel.sharedInstance()
            
            txtFieldAddressLine1.text = deliveryAddrss.flat_building_name + ", " + deliveryAddrss.address_line_1.truncateLastTwoCharacter()
            txtFieldAddressLine2.text = deliveryAddrss.address_line_2.truncateLastTwoCharacter()
            txtFieldPincode.text = deliveryAddrss.postal_code
            txtFieldLandmark.text = deliveryAddrss.landmark.truncateLastTwoCharacter()
            
            txtFieldAddressLine1.enabled = false
            txtFieldAddressLine2.enabled = false
            txtFieldPincode.enabled = false
            
        }
    }
    
    // MARK:- Custome Button Action
    
    
    //=========vegbutton=======
    @IBAction func vegButtonAction(sender: AnyObject) {
        let btn : UIButton = sender as! UIButton
        if(btn.tag==0)
        {
            btn.tag = 1;
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_selected"), forState: UIControlState.Normal)
        }
        else{
            btn.tag = 0
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_unselected"), forState: UIControlState.Normal)
            
        }
    }
    
    //=======nonveg button=======
    @IBAction func nonVegButtonAction(sender: AnyObject) {
        let btn : UIButton = sender as! UIButton
        if(btn.tag==0)
        {
            btn.tag = 1;
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_selected"), forState: UIControlState.Normal)
        }
        else{
            btn.tag = 0
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_unselected"), forState: UIControlState.Normal)
            
        }
        
    }
    
    @IBAction func addLocationButtonAction(sender: AnyObject) {
        
        let chooseAnAddressViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMLChooseAnAddressViewController") as? HMLChooseAnAddressViewController
        self.navigationController?.pushViewController(chooseAnAddressViewController!, animated: true)
        
        
    }
    //===========submittbutton========
    @IBAction func submitButtonAction(sender: AnyObject) {
        
        let veg: Bool = btnVeg.tag==1 ? true: false
        let nonVeg: Bool = btnNonVeg.tag==1 ? true: false
        
        
        if(!HMLValidation.validateUserDetail(txtFieldAddressLine1.text!, landMark: txtFieldLandmark.text!, pincode: txtFieldPincode.text!,veg: veg, nonVeg: nonVeg,lat: addressComponent.latitude,long: addressComponent.longitude,isFromConfirmYourAddressScreen: addressComponent.isFromConfirmYourAddressScreen))
        {
            return;
        }
        
       self.view.showLoader()
       var contact = ""
      if signUpUserDic?.count == 0{
        contact = HMLUtlities.getUserContactNumber()
      }
      else
      {
        contact = signUpUserDic!["mobile"] as! String
      }

       
        let parameters = [
            "mobileNumber": contact,
            "address":[
                "userID":contact,
                "addrField1":txtFieldAddressLine1.text,
                "addrField2":txtFieldAddressLine2.text,
                "country":addressComponent.country_short_name,
                "pincode":txtFieldPincode.text,
                "landmark":txtFieldLandmark.text,
                "locality":deliveryAddrss.locality,
                "latitude":addressComponent.latitude,
                "longitude":addressComponent.longitude,
                
            ],
            "preferences":[
                "veg":veg,
                "nonVeg":nonVeg
            ]
        ]
        
        HMLAppServices.sharedInstance().postServiceRequest(urlString:HMLServiceUrls.getCompleteUrlFor(Url_First_Detail), parameter: parameters, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            let response : NSDictionary = responseData as! NSDictionary
            
            let boolValue = response["updateSuccess"] as? Bool
            if boolValue == false {
                self.showErrorMessage(response["message"] as! String)
            }
            else
            {
            //navigate to home page
                let contact = response["mobile"] as! String
                HMLUtlities.setUserInSignUpProcess(false)
                HMLUtlities.setUserFromAddressScreen(false)
                HMLUtlities.saveUserContactNumber(contact)
                HMLUtlities.showSuccessMessage(response["message"] as! String)
            self.navigateToHomeViewController()
            }
            
            print(response)
            
                       
            
            }) { (errorMessage) -> () in
                
                self.view.hideLoader()
                print(errorMessage)
                
        }
        
        
    }
    
    
    
    
}
