//
//  HMLCheckoutViewController.swift
//  Homli
//
//  Created by Daffolap-21 on 13/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLCheckoutViewController: HMLBaseViewController,UITableViewDataSource,UITableViewDelegate, RazorpayPaymentCompletionProtocol {
    let placeOrderModel = HMLPlaceOrderModel.sharedInstance()
    private var razorpay : Razorpay!
    @IBOutlet weak var tableView: UITableView!
    var deliverySlotArrayTo=[String]()
    var deliverySlotArrayFrom=[String]()
    let paymentTitles = ["Dilevery Time","Payment", "Promo Code"]
    let imagesForPayment = ["ic_watch", "ic_payment", "ic_account_free_deliveries"]
    var orderId = 0
    var orderSuccessFlag  = false
    override func viewDidLoad() {
        super.viewDidLoad()
        HMLPlaceOrderModel.sharedInstance().selectedPaymentModeIndex = nil
        HMLPlaceOrderModel.sharedInstance().paymentMethod = nil
        HMLPlaceOrderModel.sharedInstance().promoCode = nil
        HMLPlaceOrderModel.sharedInstance().discountAmount = 0.0
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.title = "CHECKOUT"
        self.setBackButton()
        tableView.removeEmptyRows()
        print("plates info: \(HMLPlaceOrderModel.sharedInstance().orderItems)")
        self.tableView.registerNib(UINib(nibName: "HMLCartItemTableViewCell", bundle: nil), forCellReuseIdentifier: "CartItemCell")
        self.callServiceForDeliverySlots()
        
        let userDetailsDic = HMLUtlities.getSignInUserDetail()
        let userAddress = userDetailsDic["address"]!
        HMLPlaceOrderModel.sharedInstance().deliveryAddressId = "\(userAddress["address_ID"]!!)"
        razorpay = Razorpay.initWithKey(PUBLIC_KEY_FOR_RAZOR_PAY, andDelegate: self, forViewController: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return 10
        
        if(section == 0)
        {
            return 1
        }
        else if (section == 1)
        {
            return 3
        }
        else
        {
            return HMLPlaceOrderModel.sharedInstance().orderItems.count + 1
        }
        
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 50))
        footerView.backgroundColor = ColorFromHexaCode(0xF6F6F6)
        //footerView.backgroundColor = UIColor.redColor()
        var lineView = UIView(frame: CGRectMake(0, 0, footerView.frame.size.width, 1))
        lineView.backgroundColor = UIColor.lightGrayColor()
        footerView.addSubview(lineView)
        
        lineView = UIView(frame: CGRectMake(0, footerView.frame.size.height-1, footerView.frame.size.width, 1))
        lineView.backgroundColor = UIColor.lightGrayColor()
        footerView.addSubview(lineView)
        
        let lblTitle = UILabel()
        lblTitle.frame = CGRectMake(10, 10 , footerView.frame.size.width-50, 30)
        footerView.addSubview(lblTitle)
        lblTitle.font = UIFont(name: "OpenSans", size: 18)
        var title = ""
        
        if(section == 0)
        {
            title = "DELIVERY ADDRESS"
        }
        else if(section == 1)
        {
            title = "ORDER DETAILS"
        }
        else if(section == 2)
        {
            title = "ORDER SUMMARY"
        }
        lblTitle.text = title
        
        return footerView
        
        
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let reuseIdentifier = "CheckoutCell"
        if(indexPath.section == 0)
        {
            let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as UITableViewCell
            let userDetailsDic = HMLUtlities.getSignInUserDetail()
            let userAddress = userDetailsDic["address"]!
            var deliveryAddress = HMLPlaceOrderModel.sharedInstance().deliveryAddress
            print(userAddress)
            print(deliveryAddress)
            
            if deliveryAddress == nil{
                deliveryAddress = userAddress as? NSDictionary
            }
            cell.textLabel?.text = "\(deliveryAddress!["addrField1"]!) \(deliveryAddress!["addrField2"]!)"
            cell.textLabel?.numberOfLines = 0
            
            cell.textLabel?.font = UIFont(name: "OpenSans-Regular", size: 14)
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.layoutIfNeeded()
            return cell
        }
        else if(indexPath.section == 1)
        {
            var cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as UITableViewCell
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle,
                                   reuseIdentifier: reuseIdentifier)
            cell.textLabel?.text = paymentTitles[indexPath.row]
            cell.imageView?.image = UIImage(named: "\(imagesForPayment[indexPath.row])")
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            
            if(indexPath.row == 0 && HMLPlaceOrderModel.sharedInstance().deliverySlot != nil)
            {
                cell.detailTextLabel?.text = "\(HMLPlaceOrderModel.sharedInstance().deliverySlot!)"
            }
            else if (indexPath.row == 1 && HMLPlaceOrderModel.sharedInstance().paymentMethod != nil)
            {
                
                cell.detailTextLabel?.text = HMLPlaceOrderModel.sharedInstance().paymentMethod! == PaymentMethod.COD.rawValue ? "Cash On Delivery(COD)" : "Net Banking/Card/Wallet  "
            }
            else if (indexPath.row == 2){
                if ( HMLPlaceOrderModel.sharedInstance().promoCode != nil)
                {
                    cell.detailTextLabel?.text = "\(HMLPlaceOrderModel.sharedInstance().promoCode!)"
                }
            }
            
            cell.layoutIfNeeded()
            return cell
        }
        else
        {
            if (indexPath.row < HMLPlaceOrderModel.sharedInstance().orderItems.count)
            {
                let cell = tableView.dequeueReusableCellWithIdentifier("CartItemCell", forIndexPath: indexPath) as! HMLCartItemTableViewCell
                
                let plateItem = HMLPlaceOrderModel.sharedInstance().orderItems[indexPath.row] as HMLPlateDetailModel
                cell.configureCell(indexPath, item: plateItem)
                cell.indicatorImageView.hidden = true
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                cell.layoutIfNeeded()
                return cell
            }
            else
            {
                if (HMLPlaceOrderModel.sharedInstance().promoCode != nil)
                {
                    let cell = tableView.dequeueReusableCellWithIdentifier("SubTotalCell", forIndexPath: indexPath) as! HMLSubTotalTableViewCell
                    cell.configureCellWithSubtotal(HMLPlaceOrderModel.sharedInstance())
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    cell.layoutIfNeeded()
                    return cell
                }
                else
                {
                    let cell = tableView.dequeueReusableCellWithIdentifier("TotalCell", forIndexPath: indexPath) as! HMLTotalTableViewCell
                    cell.totalLabel.text = String(HMLPlaceOrderModel.sharedInstance().totalAmount).setIndianCurrencyStyle()
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    cell.layoutIfNeeded()
                    return cell
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if(indexPath.section == 1 && indexPath.row == 0)
        {
            let vc = self.storyboard!.instantiateViewControllerWithIdentifier("HMLAvailableDeliverySlotViewController") as? HMLAvailableDeliverySlotViewController
            vc?.deliverySlotArrayFrom = self.deliverySlotArrayFrom
            vc?.deliverySlotArrayTo = self.deliverySlotArrayTo
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else if(indexPath.section == 1 && indexPath.row == 1)
        {
            let vc = self.storyboard!.instantiateViewControllerWithIdentifier("HMLSelectPaymentMethodViewController") as? HMLSelectPaymentMethodViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
            
        else if(indexPath.section == 1 && indexPath.row == 2)
        {
            print("promo tapped")
            let promoAlertView :  HMLPromoCodeView = self.view.loadFromNibNamed("HMLPromoCodeView") as! HMLPromoCodeView
            promoAlertView.frame = self.view.bounds
            promoAlertView.applyButton.addTarget(self, action: #selector(HMLCheckoutViewController.didTapApplyPromoButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            self.view.addSubview(promoAlertView)
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    //MARK: Razor pay delegates
    func onPaymentSuccess(payment_id: String) {
        
        //UIAlertView.init(title: "Payment Successful", message: payment_id, delegate: self, cancelButtonTitle: "OK")
        orderSuccessFlag = true
        updatePaymentReferenceId(payment_id, status: "-1", message: "Payment successful")
        self.navigationController?.navigationBarHidden = false
    }
    
    func onPaymentError(code: Int32, description str: String) {
        // UIAlertView.init(title: "Error", message: str, delegate: self, cancelButtonTitle: "OK").show()
        orderSuccessFlag = false
        updatePaymentReferenceId("NA", status: "\(code)", message: str)
        self.navigationController?.navigationBarHidden = false
    }
    
    func showPaymentForm() {
        
        let prefill = [
            "email" : "\(HMLUtlities.getUserEmailId())",
            "contact" : "\(HMLUtlities.getUserContactNumber())",
            ]
        
        let options = [
            "amount" : NSNumber(double: 100*placeOrderModel.payAmount),
            "description" : "Lunch",
            "currency" : "INR",
            "image" : Url_For_Default_Icon,
            "name" : "Homli Food Corp",
            "prefill" : prefill,
            ]
        
        debugPrint(options)
        
        self.navigationController?.navigationBarHidden = true
        razorpay.open(options as [NSObject : AnyObject])
    }
    
    
    //MARK: Button events
    
    @IBAction func didTapConfirmButton(sender: AnyObject) {
        
        if HMLPlaceOrderModel.sharedInstance().paymentMethod != nil
        {
            self.callServiceForPlaceOrder()
        }
        else
        {
            self.showErrorMessage("Please select payment mode")
        }
    }
    
    
    func didTapApplyPromoButton(sender: UIButton)
    {
        let promoAlertView = sender.superview?.superview?.superview as! HMLPromoCodeView
        let promocode = promoAlertView.promoTextField.text
        if(promocode?.trim().characters.count > 0)
        {
            print("promo code : \(promocode)")
            self.callServiceToGetPromocode(promocode!)
            promoAlertView.removeFromSuperview()
        }
        else
        {
            self.showErrorMessage("Enter promo code")
        }
    }
    
    //MARK: Web Service methods
    func callServiceToGetPromocode(promocode: String)
    {
        self.view.showLoader()
        let contactNumber = HMLUtlities.getUserContactNumber()
        var amount = 0.0
        if let _ = HMLPlaceOrderModel.sharedInstance().totalAmount
        {
            amount = HMLPlaceOrderModel.sharedInstance().totalAmount!
        }
        else
        {
            amount = 0.0
        }
        
        let parameters = [
            "mobileNumber": "\(contactNumber)",
            "promoCode": "\(promocode)",
            "orderAmount":"\(amount)"
        ]
        
        let url : String = HMLServiceUrls.getCompleteUrlFor(Url_Get_Promo_code)
        
        HMLAppServices.sharedInstance().postServiceRequest(urlString:url, parameter: parameters, successBlock: { (responseData) -> () in
            
            let response : NSDictionary = responseData as! NSDictionary
            print(response)
            self.view.hideLoader()
            print(response)
            let statusCode = response.objectForKey("status") // ["status"].IntValue
            
            if (statusCode?.integerValue != 200)
            {
                self.showErrorMessage(response["message"] as! String)
            }
            else{
                self.showSuccessMessage("Successfully applied the promocode")
                var discount : Double = 0.0
                discount = response.objectForKey("amount")!.doubleValue
                HMLPlaceOrderModel.sharedInstance().discountAmount = discount
                HMLPlaceOrderModel.sharedInstance().promoCode = "\(response.objectForKey("promoCode") as! NSString)"
                print(response.objectForKey("promoCode") as! NSString)
                self.tableView.reloadData()
            }
            
        }) { (errorMessage) -> () in
            
            self.view.hideLoader()
            self.showErrorMessage(errorMessage.localizedDescription)
        }
    }
    
    let dic = [["startTime":"10:00 AM","endTime":"11:00 AM"],["startTime":"11:00 AM","endTime":"12:00 PM"],["startTime":"12:00 PM","endTime":"01:00 PM"],["startTime":"01:00 PM","endTime":"02:00 PM"],["startTime":"02:00 PM","endTime":"03:00 PM"]]
    
    func callServiceForDeliverySlots()
    {
        
        self.view.showLoader()
        let urlStr = HMLServiceUrls.getCompleteUrlFor(Url_payment_Method)
        HMLAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
            let response: NSArray = responseData as! NSArray
            print(response)
            var index = 0
            for i  in  0..<response.count
            {
                let st = response.objectAtIndex(i) as! NSDictionary
                if st["isDefault"] as! Bool{
                    index = i
                }
                self.deliverySlotArrayTo.append(st["startTime"] as! String)
                self.deliverySlotArrayFrom.append(st["endTime"] as! String)
            }
            if (response.count > 0)
            {
                
                HMLPlaceOrderModel.sharedInstance().deliverySlotStartTime = self.deliverySlotArrayTo[index]
                HMLPlaceOrderModel.sharedInstance().deliverySlotEndTime = self.deliverySlotArrayFrom[index]
                HMLPlaceOrderModel.sharedInstance().deliverySlot = "\(self.deliverySlotArrayTo[index]) - \(self.deliverySlotArrayFrom[index])"
                HMLPlaceOrderModel.sharedInstance().selectedSlotIndex = index
            }
            self.tableView.reloadData()
            
            self.view.hideLoader()
        }) { (errorMessage) -> () in
            print(errorMessage.localizedDescription)
            self.view.hideLoader()
            self.showErrorMessage(errorMessage.localizedDescription as String)
        }
    }
    
    func callServiceForPlaceOrder()
    {
        self.view.showLoader()
        
        let contactNumber = HMLUtlities.getUserContactNumber()
        var amount = 0.0
        if let _ = HMLPlaceOrderModel.sharedInstance().totalAmount
        {
            amount = HMLPlaceOrderModel.sharedInstance().totalAmount!
        }
        else
        {
            amount = 0.0
        }
        
        var orderItems:Array = [AnyObject]()
        for order in HMLPlaceOrderModel.sharedInstance().orderItems
        {
            
            let item = [
                "plateCode": "\(order.plateCode!)",
                "numberOfPlates":"\(order.noOfPlates)",
                "platePrice":"\(order.price!)"
            ]
            
            orderItems.append(item)
        }
        placeOrderModel.payAmount = amount-placeOrderModel.discountAmount
        let paymentInfo = ["paymentMethod":"\(HMLPlaceOrderModel.sharedInstance().paymentMethod!)"]
        let parameters = [
            "userMobile":"\(contactNumber)",
            "deliveryAddressId":"\(HMLPlaceOrderModel.sharedInstance().deliveryAddressId!)",
            "promoCode":  placeOrderModel.promoCode == nil ? "" : "\(placeOrderModel.promoCode as! String)",
            "totalAmount":"\(amount)",
            "discountAmount":"\(placeOrderModel.discountAmount)",
            "payAmount": "\(placeOrderModel.payAmount)",
            "deliverySlotStartTime":"\(HMLPlaceOrderModel.sharedInstance().deliverySlotStartTime as! String)".getDateFormTimeString(),
            "deliverySlotEndTime":"\(HMLPlaceOrderModel.sharedInstance().deliverySlotEndTime as! String)".getDateFormTimeString(),
            "paymentInfo":paymentInfo,
            "orderItems":orderItems
        ]
        
        print("request data: \(parameters)")
        
        let url : String = HMLServiceUrls.getCompleteUrlFor(Url_Create_Order)
        HMLAppServices.sharedInstance().postServiceRequest(urlString:url, parameter: parameters, successBlock: { (responseData) -> () in
            let response : NSDictionary = responseData as! NSDictionary
            self.view.hideLoader()
            print(response)
            let responseStatus = response["orderID"]
            
            if (responseStatus != nil) {
                
                if self.placeOrderModel.paymentMethod == PaymentMethod.COD.rawValue{
                    self.showOrderPlacedSuccessAlertView()
                }
                else
                {
                    self.orderId = response["orderID"] as! Int
                    self.showPaymentForm()
                }
            }
            else{
                HMLUtlities.showErrorMessage(response["errorMessage"] as! String)
            }
            
        }) { (errorMessage) -> () in
            
            self.view.hideLoader()
            HMLUtlities.showErrorMessage(errorMessage.localizedDescription)
        }
    }
    func updatePaymentReferenceId(refId:String,status:String,message:String){
        
        let parameters = [
            "orderId":"\(orderId)",
            "orderPaymentRefId":refId,
            "transactionStatus":status,
            "transactionMessage":message,
            ]
        print(parameters)
        HMLAppServices.postServiceRequestWithNoResponse(urlString: HMLServiceUrls.getCompleteUrlFor(Url_Update_Payment_RefId), parameter: parameters, successBlock: { (responseData) -> () in
            
            if let statusCode = responseData.statusCode {
                if statusCode == 200 {
                    if self.orderSuccessFlag {
                        self.showOrderPlacedSuccessAlertView()
                    }
                    else{
                        self.showOrderPlacedFailedAlertView()
                    }
                }
            }
            
            
        }) { (errorMessage) -> () in
            
            
        }
        
        
    }
    
    func showOrderPlacedSuccessAlertView()
    {
        placeOrderModel.initWithDefaultValue()
        let alert=UIAlertController(title: "", message: Order_placed_successful, preferredStyle: UIAlertControllerStyle.Alert);
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            
            self.popToViewController(HMLYourOrderViewController)
            
        }))
        presentViewController(alert, animated: true, completion: nil);
        
    }
    func showOrderPlacedFailedAlertView()
    {
        
        let alert=UIAlertController(title: "", message: Order_placed_failed, preferredStyle: UIAlertControllerStyle.Alert);
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            
            
        }))
        presentViewController(alert, animated: true, completion: nil);
        
    }
    
    
}


