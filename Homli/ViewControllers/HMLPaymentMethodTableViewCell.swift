//
//  HMLPaymentMethodTableViewCell.swift
//  Homli
//
//  Created by Jenkins on 4/13/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLPaymentMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var radioImg: UIImageView!
    @IBOutlet weak var detailArrayLabel: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
