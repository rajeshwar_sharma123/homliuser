//
//  HMLYourCartViewController.swift
//  Homli
//
//  Created by Daffolap-21 on 12/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLYourCartViewController: HMLBaseViewController, UITableViewDelegate, UITableViewDataSource,HMLYourCartUpdateViewDelegate,HMLLetsEatViewDelegate {

  @IBOutlet weak var tableView: UITableView!
    var totalPrice : Double = 0.00
    @IBOutlet var lblTotalPrice: UILabel!
    var letsEatView : HMLLetsEatView!
    var yourCartUpdateView : HMLYourCartUpdateView!
    override func viewDidLoad() {
        super.viewDidLoad()
      self.setBackButton()
      self.title = "YOUR CART"
      setletsEatView()
      self.tableView.delegate = self
      self.tableView.dataSource = self
      self.tableView.estimatedRowHeight = 74
      self.tableView.tableFooterView = UIView.init(frame: CGRectZero)
      lblTotalPrice.text = String(HMLPlaceOrderModel.sharedInstance().totalAmount).setIndianCurrencyStyle()
       print("plates info: \(HMLPlaceOrderModel.sharedInstance().orderItems)")
      self.tableView.registerNib(UINib(nibName: "HMLCartItemTableViewCell", bundle: nil), forCellReuseIdentifier: "CartItemCell")
      setSideMenuPanModeNone()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setletsEatView(){
        letsEatView = HMLLetsEatView.initLetsEatView(self.view)
        letsEatView.delegate = self
        letsEatView.hidden = true
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

  // MARK: - Table View Data Source & Delegate Method

  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }

  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }


  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // return 10
    if HMLPlaceOrderModel.sharedInstance().orderItems.count == 0{
        
        letsEatView.hidden = false
    }
    else{
        letsEatView.hidden = true
    }

    return HMLPlaceOrderModel.sharedInstance().orderItems.count
  }

  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
  {

    let cell = tableView.dequeueReusableCellWithIdentifier("CartItemCell", forIndexPath: indexPath) as! HMLCartItemTableViewCell
    let plateItem = HMLPlaceOrderModel.sharedInstance().orderItems[indexPath.row] as HMLPlateDetailModel
    cell.configureCell(indexPath, item: plateItem)
    cell.layoutIfNeeded()
    return cell
  }


  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    tableView.deselectRowAtIndexPath(indexPath, animated: false)
    let plateItem = HMLPlaceOrderModel.sharedInstance().orderItems[indexPath.row] as HMLPlateDetailModel
    yourCartUpdateView = HMLYourCartUpdateView.initYourCartUpdateView(self.view,plateDetail: plateItem,atIndex: indexPath.row)
    yourCartUpdateView.delegate = self
}

  @IBAction func didTapCheckoutButton(sender: AnyObject) {

    let vc = self.storyboard!.instantiateViewControllerWithIdentifier("HMLCheckoutViewController") as? HMLCheckoutViewController
    
    self.navigationController?.pushViewController(vc!, animated: true)
  }
    //MARK:- HMLYourCartUpdateViewDelegate Method
    func updateNumberOfPlate() {
        lblTotalPrice.text = String(HMLPlaceOrderModel.sharedInstance().totalAmount).setIndianCurrencyStyle()
        self.tableView.reloadData()
    }
    
    //MARK:- HMLLetsEatViewDelegate Method
    func letsEatAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
