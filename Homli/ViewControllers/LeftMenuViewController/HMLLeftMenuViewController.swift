//
//  HMLLeftMenuViewController.swift
//  Homli
//
//  Created by daffolapmac on 17/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import MFSideMenu
class HMLLeftMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
	@IBOutlet var tableView: UITableView!
	var menuName = []

	@IBOutlet var userImgView: UIImageView!
	@IBOutlet var userName: UILabel!
	var yourOrderVC: HMLYourOrderViewController!
	// MARK:LifeCycle method
	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
		menuName = ["Home", "Profile", "History", "Referrals", "Contact Us", "Help", "Signout"]
		self.view.backgroundColor = ColorFromHexaCode(k_Side_Menu_Color)
		userImgView.layer.cornerRadius = userImgView.frame.size.height / 2
		userImgView.clipsToBounds = true
		userImgView.layer.borderWidth = 3
		userImgView.layer.borderColor = ColorFromHexaCode(k_Border_Color).CGColor
	}
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)

		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HMLLeftMenuViewController.menuStateEventOccurred(_:)), name: MFSideMenuStateNotificationEvent, object: nil)
	}

	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)

		NSNotificationCenter.defaultCenter().removeObserver(self, name: MFSideMenuStateNotificationEvent, object: nil)
	}

	// ************ This method get call whenever side menu open or close *************
	func menuStateEventOccurred(notification: NSNotification) {

		if notification.name == MFSideMenuStateNotificationEvent {

			userName.text = HMLUtlities.getUserName()

		}
	}
	// MARK: - TableView Data Source & Delegates Method
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return menuName.count
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
	{

		let cell = tableView.dequeueReusableCellWithIdentifier("HMLLeftMenuCell", forIndexPath: indexPath) as! HMLLeftMenuCell
		cell.selectionStyle = UITableViewCellSelectionStyle.None
		cell.imgView.image = getIconForSideMenuOption(atIndex: indexPath.row)
		cell.imgView.setImageColorFromHexaCode(k_Side_Menu_Default_Color)
		cell.lblName.text = menuName[indexPath.row] as? String
		cell.lblName.textColor = ColorFromHexaCode(k_Side_Menu_Default_Color)

		return cell
	}
	func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
		let cell: HMLLeftMenuCell = tableView.cellForRowAtIndexPath(indexPath) as! HMLLeftMenuCell
		cell.imgView.setImageColorFromHexaCode(k_Theme_Color)
		cell.lblName.textColor = ColorFromHexaCode(k_Theme_Color)

	}

	func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
		let cell: HMLLeftMenuCell = tableView.cellForRowAtIndexPath(indexPath) as! HMLLeftMenuCell

		cell.imgView.setImageColorFromHexaCode(k_Side_Menu_Default_Color)
		cell.lblName.textColor = ColorFromHexaCode(k_Side_Menu_Default_Color)
	}

	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
	{

		return 60
	}
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		var viewController = UIViewController()

		switch (indexPath.row)
		{
			case 0:

			viewController = (self.storyboard!.instantiateViewControllerWithIdentifier("HMLYourOrderViewController") as? HMLYourOrderViewController)!
			case 1:

			viewController = (self.storyboard!.instantiateViewControllerWithIdentifier("HMLAccountViewController") as? HMLAccountViewController)!

			case 2:
			print("")
			viewController = (self.storyboard!.instantiateViewControllerWithIdentifier("HMLHistoryViewController") as? HMLHistoryViewController)!
			case 3:
			viewController = (self.storyboard!.instantiateViewControllerWithIdentifier("HMLReferralViewController") as? HMLReferralViewController)!
			case 4:
			viewController = (self.storyboard!.instantiateViewControllerWithIdentifier("HMLContactUsViewController") as? HMLContactUsViewController)!
			case 5:
			print("")
			return
			case 6:
			showAlertView()
			return
			default:
			print("")

		}
		self.menuContainerViewController.centerViewController.pushViewController(viewController, animated: true)
		self.toggleLeftSide()

	}

	// ********** Method to get image for side menu option **********
	func getIconForSideMenuOption(atIndex index: NSInteger) -> UIImage
	{

		var icnName = String()

		switch (index)
		{
			case 0:
			icnName = "ic_home_white"
			case 1:
			icnName = "icn_slidemenu_profile"
			case 2:
			icnName = "icn_slidemenu_history"
			case 3:
			icnName = "icn_slidemenu_referral"
			case 4:
			icnName = "icn_slidemenu_contact"
			case 5:
			icnName = "ic_help"
			case 6:
			icnName = "icn_slidemenu_logout"
			default:
			print("")

		}
		return UIImage(named: icnName)!
	}

	// MARK:This method Show the AlertView
	func showAlertView()
	{

		let alert = UIAlertController(title: "Confirm Sign Out", message: "\(HMLUtlities.getUserName()), you are signing out of Homli app on this device", preferredStyle: UIAlertControllerStyle.Alert);

		// no event handler (just close dialog box)
		alert.addAction(UIAlertAction(title: "Signout", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
			HMLPlaceOrderModel.sharedInstance().initWithDefaultValue()
			HMLUtlities.signOutFromApp()
			let joinUsViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMLJoinUsViewController") as? HMLJoinUsViewController
			self.menuContainerViewController.centerViewController = UINavigationController(rootViewController: joinUsViewController!)
			self.toggleLeftSide()

			}))
		// event handler with closure
		alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) in

			self.closeSideMenu()

			}));
		presentViewController(alert, animated: true, completion: nil);

	}
	// MARK:This method close the Side Menu
	func closeSideMenu()
	{
		self.menuContainerViewController.setMenuState(MFSideMenuStateClosed, completion: { () -> Void in

		})
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	/*
	 // MARK: - Navigation

	 // In a storyboard-based application, you will often want to do a little preparation before navigation
	 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	 // Get the new view controller using segue.destinationViewController.
	 // Pass the selected object to the new view controller.
	 }
	 */

}
