//
//  HMLOrderDetailViewController.swift
//  Homli
//
//  Created by Vishwas Singh on 3/18/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLOrderDetailViewController: HMLBaseViewController, UIScrollViewDelegate {

	@IBOutlet var segmentedControl: UISegmentedControl!
	@IBOutlet var mainScrollView: UIScrollView!
	var orderObj = Order()
	var flag = 0
    //MARK:LifeCycle method
	override func viewDidLoad()
	{
		super.viewDidLoad()
		self.title = "ORDER DETAILS"
		getOrderDetailFromServer()
		self.setBackButton()
		print(orderObj)

	}
    
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)

	}
	// MARK:- Custom Method
	func initCustomView(orderDetail: HMLOrderDetailModel)
	{
		flag = 0

		self.mainScrollView.delegate = self

		mainScrollView.frame = CGRectMake(0, 0, screenWidth, 0)
		mainScrollView.backgroundColor = UIColor.blackColor()
		mainScrollView.contentSize = CGSizeMake(screenWidth * 2, 0)
		let statusView = NSBundle.mainBundle().loadNibNamed("HMLStatusView", owner: self, options: nil)[0] as! HMLStatusView
		statusView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
		statusView.initDefaultValue(orderDetail)
		self.mainScrollView.addSubview(statusView)

		let receiptView = NSBundle.mainBundle().loadNibNamed("HMLReciptView", owner: self, options: nil)[0] as! HMLReciptView
		receiptView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
		receiptView.frame.origin.x = mainScrollView.frame.size.width
		receiptView.initDefaultValue(orderDetail)
		self.mainScrollView.addSubview(receiptView)

	}

	// MARK:- UIScrollViewDelegate Method
    func scrollViewDidEndDecelerating(scrollView: UIScrollView)
	{
		print("scrolled")
		if (flag == 0)
		{
			segmentedControl.selectedSegmentIndex = 1
			indexChanged(segmentedControl)
			flag = 1
		}
		else
		{
			segmentedControl.selectedSegmentIndex = 0
			indexChanged(segmentedControl)
			flag = 0

		}
	}

	@IBAction func indexChanged(sender: UISegmentedControl) {
		if (segmentedControl.selectedSegmentIndex == 0)
		{
			mainScrollView.contentOffset = CGPoint(x: 0, y: 0)
		}
		else
		{

			mainScrollView.contentOffset = CGPoint(x: screenWidth, y: 0)
		}
	}

	// MARK:- Web Services Calling

	func getOrderDetailFromServer()
	{
		self.view.showLoader()
		let urlStr = HMLServiceUrls.getCompleteUrlFor(Url_Get_Order_Detail) + "/" + orderObj.orderId!

		HMLAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in

			print(responseData)

			let JSON: NSDictionary = responseData as! NSDictionary

			print("Error: \(JSON)")
			// let errorString: NSString = JSON["errorMessage"] as! NSString

			if JSON["errorMessage"] != nil {
				self.showErrorMessage(JSON["errorMessage"] as! String)

			}
			else {
				let orderArray = HMLOrderDetailModel.bindDataWithModel(JSON)
				self.initCustomView(orderArray[0])
			}
			self.view.hideLoader()

		}) { (errorMessage) -> () in
			print(errorMessage.localizedDescription)
			self.view.hideLoader()
			self.showErrorMessage(errorMessage.localizedDescription as String)
		}

	}
}

