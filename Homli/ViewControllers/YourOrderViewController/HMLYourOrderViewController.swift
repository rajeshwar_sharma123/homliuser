//
//  HMLYourOrderViewController.swift
//  Homli
//
//  Created by Jenkins on 3/14/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//
import UIKit
import Alamofire

class HMLYourOrderViewController: HMLBaseViewController, UITableViewDataSource {
	@IBOutlet weak var tableView: UITableView!
	var orderData = [Order]()

	// MARK: UIViewController life cycle
	var userAddressCheck = false
	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = "YOUR ORDERS"
		getUserDetailFromService()

	}
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		setSideMenuPanModeDefault()
		if HMLUtlities.isUserFromAddressScreen() {
			if !HMLUtlities.isUserAddressAvailable() {
				navigateToAddressDetailScreen()
			}
		}
		if HMLUtlities.isUserAddressAvailable() {
			userAddressCheck = true
			getOrderListFromService()
		}

	}
	// MARK:  Handle Warning
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	// MARK: - Table View Data Source & Delegate Method
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return 1
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.orderData.count
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
	{
		let order = orderData[indexPath.row] as Order
		let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! HMLYourOrderTableViewCell
		cell.configureCell(indexPath, order: order)
		cell.cancelOrder.addTarget(self, action: #selector(HMLYourOrderViewController.didTapCancelOrderButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		return cell
	}
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}
	func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

		return UITableViewAutomaticDimension
	}

	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		print(indexPath.row)

		let orderDetailViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMLOrderDetailViewController") as? HMLOrderDetailViewController
		orderDetailViewController?.orderObj = orderData[indexPath.row] as Order
		self.navigationController?.pushViewController(orderDetailViewController!, animated: true)
	}

	// MARK:- Tap Gesture Action

	@IBAction func placeAnOrderTapGestureAction(sender: AnyObject) {
		navigateToMenuViewController()
	}
	func navigateToMenuViewController() {
		let menuViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMLMenuViewController") as? HMLMenuViewController
		self.navigationController?.pushViewController(menuViewController!, animated: true)
	}
	func didTapCancelOrderButton(sender: UIButton)
	{
		let alert = UIAlertController(title: "", message: "Are you sure that you want to cancel the order?", preferredStyle: UIAlertControllerStyle.Alert);

		alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in

			let tappedIndex = sender.tag
			let order = self.orderData[tappedIndex] as Order
			self.callServiceToCancelOrder(tappedIndex, orderId: order.orderId!)

			}))
		alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) in

			}));
		presentViewController(alert, animated: true, completion: nil);

	}

	func navigateToAddressDetailScreen()
	{
		let addressDetailViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMLAddressDetailViewController") as? HMLAddressDetailViewController
		self.navigationController?.pushViewController(addressDetailViewController!, animated: false)
	}

	// MARK: Web Service methods
	func callServiceToCancelOrder(tappedIndex: Int, orderId: String)
	{
		self.view.showLoader()
		let parameters = [
			"orderId": "\(orderId)",

		]
		let url: String = HMLServiceUrls.getCompleteUrlFor(Url_To_Cancel_Order)
		HMLAppServices.sharedInstance().postServiceRequest(urlString: url, parameter: parameters, successBlock: { (responseData) -> () in
			let response: NSDictionary = responseData as! NSDictionary
			self.view.hideLoader()
			print(response)
			let statusCode = response.objectForKey("status")
			if (statusCode?.integerValue == 200)
			{
				HMLUtlities.showSuccessMessage("Order canceled successfully.")
				self.orderData.removeAtIndex(tappedIndex)
				self.tableView.reloadData()
			}
			else {
				HMLUtlities.showErrorMessage("Can't cancel order.")

			}
		}) { (errorMessage) -> () in

			self.view.hideLoader()
			self.showErrorMessage(errorMessage.localizedDescription)
		}
	}
	func getUserDetailFromService()
	{
		let urlStr = HMLServiceUrls.getCompleteUrlFor(Url_Read_User_Detail) + "/\(HMLUtlities.getUserContactNumber())"

		HMLAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in

			print(responseData)

			let userProfile: NSDictionary = responseData as! NSDictionary
			HMLUtlities.saveUserProfile(userProfile)

			if !HMLUtlities.isUserAddressAvailable() {
				self.navigateToAddressDetailScreen()
			}
			if HMLUtlities.isUserAddressAvailable() && !self.userAddressCheck {
				self.getOrderListFromService()
			}

		}) { (errorMessage) -> () in
		}
	}

	/**
	 A function will called when page is loaded
	 */
	func getOrderListFromService()
	{
		self.view.showLoader()
		let urlStr = HMLServiceUrls.getCompleteUrlFor(Url_Get_Order_List) + "/\(HMLUtlities.getUserContactNumber())"

		HMLAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
			self.view.hideLoader()
			print(responseData)
			let JSON: NSDictionary = responseData as! NSDictionary
			let orderFromService = JSON["orders"] as? NSArray
			self.orderData = Order.getOrderFromJSON(orderFromService!)
			if self.orderData.count == 0 {
				self.navigateToMenuViewController()
			}
			else {

				self.tableView.reloadData()
			}

		}) { (errorMessage) -> () in
			print(errorMessage.localizedDescription)
			self.view.hideLoader()
			self.showErrorMessage(errorMessage.localizedDescription as String)
		}
	}

}
