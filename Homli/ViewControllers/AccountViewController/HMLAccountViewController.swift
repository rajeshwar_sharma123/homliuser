//
//  HMLAccountViewController.swift
//  Homli
//
//  Created by Vishwas Singh on 3/18/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLAccountViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
  @IBOutlet var tableView: UITableView!
  var dataArray  = []
   //MARK:LifeCycle method 
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "ACCOUNT"
    self.tableView.registerNib(UINib(nibName: "HMLAccountCell", bundle: nil), forCellReuseIdentifier: "HMLAccountCell")
    let backButton = UIButton(type: UIButtonType.Custom)
    backButton.addTarget(self, action: #selector(HMLAccountViewController.didTapBackButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    backButton.setImage(UIImage(named: "ic_action_close") , forState: UIControlState.Normal)
    backButton.sizeToFit()
    let backButtonItem = UIBarButtonItem(customView: backButton)
    self.navigationItem.leftBarButtonItem = backButtonItem
    setSideMenuPanModeNone()
  
  }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        dataArray = [[
            
            ["title" : "\(HMLUtlities.getUserName())","description" : "Change your account informartion","image" : "icn_default"],
            ["title" : "Addresss","description" : "Add or remove a delivery Address","image" : "ic_menu_location"]
            ],
            
            [
                ["title" : "Delivery Support","description" : "+919167001994","image" : "ic_account_message"],
                ["title" : "Get free deliveries","description" : "","image" : "ic_account_free_deliveries"],
                ["title" : "Preferences","description" : "Change your preferences","image" : "ic_account_preferences"]
            ]
]
        tableView.reloadData()
    }

  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 2
  }
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0{
      return 2
    }
    return 3
  }


  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let arry = dataArray[indexPath.section]

    let cell: HMLAccountCell! = tableView.dequeueReusableCellWithIdentifier("HMLAccountCell") as? HMLAccountCell
    cell.configureCell(indexPath, arrayData: arry as! NSArray)
    return cell

  }


  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return  60

  }
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if section == 0{
      let headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 30))
      headerView.backgroundColor = ColorFromHexaCode(0xF6F6F6)
      let lineView = UIView(frame: CGRectMake(0, headerView.frame.size.height-1, headerView.frame.size.width, 1))
      lineView.backgroundColor = UIColor.lightGrayColor()
      headerView.addSubview(lineView)

      return headerView
    }
    return UIView()

  }
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == 0{
      return 30
    }
    return 0
  }
  func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

    let footerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 30))
    footerView.backgroundColor = ColorFromHexaCode(0xF6F6F6)
    var lineView = UIView(frame: CGRectMake(0, 0, footerView.frame.size.width, 1))
    lineView.backgroundColor = UIColor.lightGrayColor()
    footerView.addSubview(lineView)

    lineView = UIView(frame: CGRectMake(0, footerView.frame.size.height-1, footerView.frame.size.width, 1))
    lineView.backgroundColor = UIColor.lightGrayColor()
    footerView.addSubview(lineView)


    return footerView

  }

  func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 30
  }

  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    if( indexPath.section == 0 && (indexPath.row) == 0)
    {

      let editAccountViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HMLEditAccountViewController") as! HMLEditAccountViewController
      self.navigationController?.pushViewController(editAccountViewController, animated: true)
    }
    else if(indexPath.section == 0 && (indexPath.row) == 1)
    {
      let vc = self.storyboard?.instantiateViewControllerWithIdentifier("HMLAddressesViewController") as! HMLAddressesViewController
        vc.fromProfile = true
      self.navigationController?.pushViewController(vc, animated: true)
    }

    else if(indexPath.section == 1 && (indexPath.row) == 0)
    {

      if let url = NSURL(string: "telprompt://+919167001994") {
        UIApplication.sharedApplication().openURL(url)
      }
      else
      {
        self.showErrorMessage("Please check for SIM card")
      }

    }
    else if(indexPath.section == 1 && (indexPath.row) == 1)
    {
      let vc = self.storyboard?.instantiateViewControllerWithIdentifier("HMLReferralViewController") as! HMLReferralViewController
      self.navigationController?.pushViewController(vc, animated: true)
    }

    else if(indexPath.section == 1 && (indexPath.row) == 2)
    {
      let vc = self.storyboard?.instantiateViewControllerWithIdentifier("HMLPreferencesViewController") as! HMLPreferencesViewController
      self.navigationController?.pushViewController(vc, animated: true)
    }

    tableView.deselectRowAtIndexPath(indexPath, animated: true)
  }

  func didTapBackButton(sender : AnyObject)
  {
    self.navigationController?.popViewControllerAnimated(true)
  }

}
