//
//  HMLAddressesViewController.swift
//  Homli
//
//  Created by Vishwas Singh on 3/23/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLAddressesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, removeRowDelegate {

	@IBOutlet var tableView: UITableView!

	@IBOutlet var addLocationView: UIView!
	var previousIndex: Int!
	var selectedAddressIndex: NSInteger = 0
	var demoIndex: NSIndexPath!
	var isSelected: Bool!
	var addressListArray = []
	var fromProfile = false
	var isCheck: Bool!
	var addressArray = [AnyObject]()
	var userDetailsDic: NSMutableDictionary = [:]
	let rightButton = UIButton(type: UIButtonType.Custom)

	// MARK:- LifeCycle Methods
	override func viewDidLoad()
	{
		super.viewDidLoad()
		self.title = "ADDRESSES"
		print("HMLAddressLocationViewController")
		tableView.dataSource = self
		tableView.delegate = self
		isCheck = false
		isSelected = false
		selectedAddressIndex = 0
		setSideMenuPanModeNone()
		userDetailsDic = HMLUtlities.getSignInUserDetail() // as! Dictionary<String, AnyObject>

		let userAddress = userDetailsDic["address"]!
		if HMLPlaceOrderModel.sharedInstance().deliveryAddress == nil {
			HMLPlaceOrderModel.sharedInstance().deliveryAddressId = "\(userAddress["address_ID"]!!)"
		}
		else {
			HMLPlaceOrderModel.sharedInstance().deliveryAddressId = "\(HMLPlaceOrderModel.sharedInstance().deliveryAddress!["address_ID"]!)"
		}
		// print
		debugPrint("user details: \(userDetailsDic) with ID \(HMLPlaceOrderModel.sharedInstance().deliveryAddressId as! String)")

		let backButton = UIButton(type: UIButtonType.Custom)
		backButton.addTarget(self, action: #selector(HMLAddressesViewController.didTapBackButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		// backButton.setTitle("TITLE", forState: UIControlState.Normal)
		backButton.setImage(UIImage(named: "ic_action_close"), forState: UIControlState.Normal)
		backButton.sizeToFit()
		let backButtonItem = UIBarButtonItem(customView: backButton)
		self.navigationItem.leftBarButtonItem = backButtonItem

		rightButton.addTarget(self, action: #selector(HMLAddressesViewController.didTapDoneButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		rightButton.frame = CGRectMake(0, 0, 22, 22)
		rightButton.setImage(UIImage(named: "ic_edit"), forState: UIControlState.Normal)
		rightButton.setImage(UIImage(named: "ic_done"), forState: UIControlState.Selected)
		rightButton.setImage(UIImage(named: "ic_done"), forState: UIControlState.Highlighted)
		/// rightButton.sizeToFit()
		let rightButtonItem = UIBarButtonItem(customView: rightButton)
		self.navigationItem.rightBarButtonItem = rightButtonItem

	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(true)
		// call service to get address list
		getAddressListFromService()
	}

	let dic = [
		[
			"addrField1": "d-234",
			"addrField2": "varanasi,UP hghhghhghghhj ghghghjg hghghjghj vhjghghjgh hjghjghjghjghjgjg",
			"address_ID": 23,
			"primary": "Y"
		],
		[
			"addrField1": "d-345",
			"addrField2": "Meerut,UP",
			"address_ID": 27,
			"primary": "N"
		],
		[
			"addrField1": "f-78",
			"addrField2": "Jaunpur,UP jhjjkhkj gghjgjhgjh  hghjghjgjh hghghjghjgjh",
			"address_ID": 27,
			"primary": "N"
		]

	]

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
	{
		self.view.hideLoader()
		let cellIdentifier: String = "CustomFields"
		var cell: HMLAddressCustomCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as?
		HMLAddressCustomCell

		if (cell == nil)
		{
			var nib: Array = NSBundle.mainBundle().loadNibNamed("HMLAddressCustomCell", owner: self, options: nil)
			cell = nib[0] as? HMLAddressCustomCell

			cell?.lblAddress.text = "demo address " // addressArray[indexPath.row] as! String
		}
		cell?.delegate = self
		cell?.removeRow.tag = indexPath.row

		var tempAddress = addressArray[indexPath.row] as! Dictionary<String, AnyObject>

		cell?.lblAddress.text = "\(tempAddress["addrField1"]!) \(tempAddress["addrField2"]!)"
		cell?.updateButton.addTarget(self, action: #selector(HMLAddressesViewController.didTapUpdateAddress(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		cell?.updateButton.tag = indexPath.row

		let isSelected = tempAddress["primary"] as! String
		if (isSelected == "Y")
		{
			previousIndex = indexPath.row
			// cell?.updateButton.selected = true
		//	let img = UIImage(named: "ic_addresses")

			cell?.updateButton.setImage(UIImage(named: "ic_addresses"), forState: UIControlState.Normal)

		}
		else
		{
			// cell?.updateButton.selected = false
			cell?.updateButton.setImage(UIImage(named: "ic_navigation"), forState: UIControlState.Normal)
			cell?.updateButton.setImageColorFromHexaCodeForNormalState(k_Light_Gray_Color)
		}

		let defaultDeliveryID = "\(HMLPlaceOrderModel.sharedInstance().deliveryAddressId!)"
		let currentDeliveryAddress = "\(tempAddress["address_ID"]!)"

		if (Int(defaultDeliveryID) == Int(currentDeliveryAddress))
		{
			cell?.lblDelivery.hidden = false
			selectedAddressIndex = indexPath.row
		}
		else
		{
			cell?.lblDelivery.hidden = true
		}
		print(indexPath)
		if (isCheck == true && (indexPath.row == selectedAddressIndex || isSelected == "Y"))
		{
			print("no deletion")
			// cell?.DeleteButtonConstarint.constant = 30
		}
		else if (isCheck == true)
		{
			cell?.DeleteButtonConstarint.constant = 30
		}
		cell?.selectionStyle = .None
		return cell!

	}

	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}

	func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

		return 57
	}

	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if fromProfile {
			return
		}

		previousIndex = selectedAddressIndex
		selectedAddressIndex = indexPath.row
		let prevIndexPath = NSIndexPath.init(forItem: previousIndex, inSection: 0)

		let tempAddress = addressArray[indexPath.row] as! Dictionary<String, AnyObject>
		HMLPlaceOrderModel.sharedInstance().deliveryAddress = tempAddress
		HMLPlaceOrderModel.sharedInstance().deliveryAddressId = "\(tempAddress["address_ID"]!)"

		tableView.reloadRowsAtIndexPaths([indexPath, prevIndexPath], withRowAnimation: UITableViewRowAnimation.None)
		HMLPlaceOrderModel.sharedInstance().initWithDefaultValue()
		self.navigationController?.popViewControllerAnimated(true)
	}

	func removeRow(didRemove: Int) {

		var tempAddress = addressArray[didRemove] as! Dictionary<String, AnyObject>
		let addressString = "\(tempAddress["addrField1"]!) \(tempAddress["addrField2"]!)"

		let alert = UIAlertController(title: "Are you sure?", message: "You want to delete \"\(addressString)\" from your address list.", preferredStyle: UIAlertControllerStyle.Alert)
		alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: { action in
			print("primary tapped cancel")
			}))

		alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { action in

			self.callServiceToDeleteOrUpdateAddress(didRemove, url: Url_To_Delete_Address)
			}))

		self.presentViewController(alert, animated: true, completion: nil)

	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return addressArray.count
	}

	func didTapUpdateAddress(sender: UIButton)
	{
		print("primary tapped")

		if self.previousIndex != nil {
			if sender.tag == self.previousIndex {
				return
			}
		}
		var tempAddress = addressArray[sender.tag] as! Dictionary<String, AnyObject>
		let addressString = "\(tempAddress["addrField1"]!) \(tempAddress["addrField2"]!)"
		let alert = UIAlertController(title: "Are you sure?", message: "You want to set \"\(addressString)\" as your primary Address.", preferredStyle: UIAlertControllerStyle.Alert)
		alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: { action in
			print("primary tapped cancel")
			}))

		alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { action in

			self.callServiceToDeleteOrUpdateAddress(sender.tag, url: Url_To_Update_Primary)
			}))

		// show the alert
		self.presentViewController(alert, animated: true, completion: nil)
	}

	@IBAction func didTapAddLocationButton(sender: AnyObject)
	{

		let vc = self.storyboard!.instantiateViewControllerWithIdentifier("HMLChooseAnAddressViewController") as? HMLChooseAnAddressViewController
		vc?.shouldPopToAddressListingVC = true
		self.navigationController?.pushViewController(vc!, animated: true)
	}

	func didTapBackButton(sender: AnyObject)
	{
		self.navigationController?.popViewControllerAnimated(true)
	}

	func didTapDoneButton(sender: AnyObject)
	{
		isCheck = !isCheck
		self.rightButton.selected = isCheck
		tableView.reloadData()
	}
	// MARK: Service calling
	func getAddressListFromService()
	{
		self.view.showLoader()
		let urlStr = HMLServiceUrls.getCompleteUrlFor(Url_Get_Address_List) + "/\(HMLUtlities.getUserContactNumber())"

		HMLAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
			self.view.hideLoader()
			print(responseData)
			self.addressArray = responseData as! [AnyObject]
			print("address list: \(self.addressArray)")

			self.tableView.reloadData()

		}) { (errorMessage) -> () in
			print(errorMessage.localizedDescription)
			self.view.hideLoader()
			self.showErrorMessage(errorMessage.localizedDescription as String)
		}
	}
	// MARK: Service Calling
	func callServiceToDeleteOrUpdateAddress(rowIndex: Int, url: String)
	{
		var tempAddress = addressArray[rowIndex] as! Dictionary<String, AnyObject>
		let parameters = [
			"mobileNumber": HMLUtlities.getUserContactNumber(),
			"address_ID": "\(tempAddress["address_ID"]!)"
		]
		print("prameters: \(parameters)")
		self.view.showLoader()
		HMLAppServices.sharedInstance().postServiceRequest(urlString: HMLServiceUrls.getCompleteUrlFor(url), parameter: parameters, successBlock: { (responseData) -> () in

			let response: NSDictionary = responseData as! NSDictionary
			self.view.hideLoader()
			print(response)
			let responseStatus = response["response"] as? String

			if responseStatus == "SUCCESS"
			{
				if url == Url_To_Delete_Address
				{
					self.addressArray.removeAtIndex(rowIndex)
					self.tableView.reloadData()
				}
				else
				{
					print("update address to primary")
					tempAddress.updateValue("Y", forKey: "primary")

					let tempUser = self.userDetailsDic.mutableCopy()

					tempUser.setValue(tempAddress, forKey: "address")
					HMLUtlities.saveUserProfile(tempUser as! NSDictionary)
					self.addressArray.removeAtIndex(rowIndex)
					self.addressArray.insert(tempAddress, atIndex: rowIndex) // append()
					HMLPlaceOrderModel.sharedInstance().deliveryAddress = tempAddress
					HMLPlaceOrderModel.sharedInstance().deliveryAddressId = "\(HMLPlaceOrderModel.sharedInstance().deliveryAddress!["address_ID"]!)"
					var prevAddress = self.addressArray[self.previousIndex] as! Dictionary<String, AnyObject>
					prevAddress.updateValue("N", forKey: "primary")
					self.addressArray.removeAtIndex(self.previousIndex)
					self.addressArray.insert(prevAddress, atIndex: self.previousIndex)
					HMLPlaceOrderModel.sharedInstance().initWithDefaultValue()
					self.tableView.reloadData()
					if !self.fromProfile {
						self.navigationController?.popViewControllerAnimated(true)
					}
				}
			}
			else {
				HMLUtlities.showErrorMessage(response["message"] as! String)
			}

		}) { (errorMessage) -> () in

			self.view.hideLoader()
			HMLUtlities.showErrorMessage(errorMessage.localizedDescription)
		}
	}
}
