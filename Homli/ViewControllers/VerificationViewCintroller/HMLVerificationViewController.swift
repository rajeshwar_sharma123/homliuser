//
//  HMLVerificationViewController.swift
//  Demo
//
//  Created by Jenkins on 3/9/16.
//  Copyright (c) 2016 daffodilsw. All rights reserved.
//

import UIKit

class HMLVerificationViewController: HMLBaseViewController {
    @IBOutlet weak var verificationNumberLabel: UILabel!
    @IBOutlet weak var textFieldOTPCode: UITextField!
    var signUpUserDic : NSMutableDictionary?
    var resendRequestCount = 0
    @IBOutlet weak var buttonVerify: UIButton!
    //MARK:method LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "VERIFICATION"
        setBackButton()
        self.textFieldOTPCode.leftPadding(0)
        let otp : String = HMLUtlities.getOTP()
        self.textFieldOTPCode.text = otp
        signUpUserDic = HMLUtlities.getSignUpUserDetail()
        self.verificationNumberLabel.text = "One Time Password has been sent to \(signUpUserDic!["mobile"]!)"
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = false
    }
    
    @IBAction func verifyButtonAction(sender: AnyObject) {
        if(!HMLValidation.validateOTP(self.textFieldOTPCode.text!))
        {
            return;
        }
        self.view.showLoader()
        let parameters = [
            "mobileNumber": signUpUserDic!["mobile"]!,
            "otp": self.textFieldOTPCode.text!,
        ]
        HMLAppServices.sharedInstance().postServiceRequest(urlString:HMLServiceUrls.getCompleteUrlFor(Url_Confirm_OTP), parameter: parameters, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            let response : NSDictionary = responseData as! NSDictionary
            
            print(response)
            
            let boolValue = response["validOTP"] as? Bool
            if boolValue == false {
                self.showErrorMessage(response["message"] as! String)
            }
            else
            {
                HMLUtlities.setUserOTPVerifiedStatus(true)
                let addressDetailViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMLAddressDetailViewController") as? HMLAddressDetailViewController
                self.navigationController?.pushViewController(addressDetailViewController!, animated: true)
            }
            
        }) { (errorMessage) -> () in
                
                self.view.hideLoader()
        }
}
    
    
    @IBAction func resendOTPButtonAction(sender: AnyObject) {
        
        resendRequestCount = resendRequestCount + 1
        
        if resendRequestCount >= 3
        {
            let resendButton =  sender as! UIButton
            resendButton.hidden = true
        }
        let contact : String = self.signUpUserDic!["mobile"] as! String
        let urlString : String = HMLServiceUrls.getCompleteUrlFor(Url_Request_OTP)+"?phone="+contact
        self.view.showLoader()
        HMLAppServices.getServiceRequest(urlString: urlString, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            print(responseData)
            
            }) { (errorMessage) -> () in
                self.view.hideLoader()
                
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 }
