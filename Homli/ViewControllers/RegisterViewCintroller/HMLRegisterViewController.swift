//
//  HMLRegisterViewController.swift
//  Homli
//
//  Created by daffolapmac on 08/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLRegisterViewController: HMLBaseViewController, UITextFieldDelegate {

	@IBOutlet weak var txtFldName: UITextField!
	@IBOutlet weak var txtFldContact: UITextField!
	@IBOutlet weak var txtFldEmail: UITextField!
	@IBOutlet weak var txtFldPassword: UITextField!
    //MARK:LifeCycle method 
	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
		txtFldContact.delegate = self
		self.title = "REGISTER"
		setBackButton()
	}
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController!.navigationBarHidden = false
		if HMLUtlities.isUserInSignUpProcess() {

			HMLUtlities.setUserInSignUpProcess(false)
		}

	}
	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		self.view.endEditing(true)
	}

	@IBAction func signUpButtonClicked(sender: AnyObject) {
		if (!HMLValidation.validateSignUp(self.txtFldName.text!, contact: self.txtFldContact.text!, email: self.txtFldEmail.text!, password: self.txtFldPassword.text!))
		{
			return;
		}

		self.view.showLoader()
		let parameters = [
			"name": self.txtFldName.text!,
			"mobileNumber": self.txtFldContact.text!,
			"email": self.txtFldEmail.text!,
			"password": self.txtFldPassword.text!,
		]

		HMLAppServices.sharedInstance().postServiceRequest(urlString: HMLServiceUrls.getCompleteUrlFor(Url_Register), parameter: parameters, successBlock: { (responseData) -> () in

			self.view.hideLoader()
			let response: NSDictionary = responseData as! NSDictionary
			print(response)

			let userExist = response["userExists"] as! Int

			if userExist == 1 {
				self.showErrorMessage(response["message"] as! String)
				return
			}

			HMLUtlities.saveSignUpUserDetail(response.mutableCopy() as! NSMutableDictionary)
			HMLUtlities.setUserInSignUpProcess(true)
			HMLUtlities.setUserOTPVerifiedStatus(false)
			HMLUtlities.setUserAddressDetailStatus(false)
			let verificationViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMLVerificationViewController") as? HMLVerificationViewController
			self.navigationController?.pushViewController(verificationViewController!, animated: true)

			self.showSuccessMessage(response["message"] as! String)

		}) { (errorMessage) -> () in

			print(errorMessage)
			// self.showErrorMessage(errorMessage as String!)
			self.view.hideLoader()

		}

	}

	func removeNullValue(dict: NSMutableDictionary) -> NSMutableDictionary
	{

		var keysToRemove = dict.allKeys
		// let x: AnyObject = NSNull()
		for i in 0 ..< keysToRemove.count {
			if (dict.objectForKey(keysToRemove[i]) == nil || dict.objectForKey(keysToRemove[i])!.classForCoder == NSNull.classForCoder())
			{
				dict.removeObjectForKey(keysToRemove[i])

			}

		}
		return dict
	}

	// MARK:- UITextFieldDelegate Method
	func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
		guard let text = textField.text else { return true }

		let newLength = text.characters.count + string.characters.count - range.length
		return newLength <= Character_Limit // Bool
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	/*
	 // MARK: - Navigation

	 // In a storyboard-based application, you will often want to do a little preparation before navigation
	 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	 // Get the new view controller using segue.destinationViewController.
	 // Pass the selected object to the new view controller.
	 }
	 */

}
