//
//  HMLHistoryViewController.swift
//  Homli
//
//  Created by Daffolap-21 on 08/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLHistoryViewController: HMLBaseViewController {

  @IBOutlet weak var tableView: UITableView!

  var orderData = []
  var noDataView : HMLNoDataView!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    self.title = "HISTORY"
    setBackButton()
    self.tableView.estimatedRowHeight = 74
    

    // Do any additional setup after loading the view, typically from a nib.
    setSideMenuPanModeNone()
    setNoDataView()
//    let path: NSString = NSBundle.mainBundle().pathForResource("historyOrder", ofType: "json")!
//    let data : NSData = try! NSData(contentsOfFile: path as String, options: NSDataReadingOptions.DataReadingMapped)
//
//    do
//    {
//
//      let json = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
//      let orderFromService = json as! NSArray
//      self.orderData = Order.getOrderFromJSON(orderFromService)
//      self.tableView .reloadData()
//    }
//
//    catch
//
//    {
//      print("error serializing JSON: \(error)")
//    }
    
    getPreviousOrderListFromService()
    
  }
    func getPreviousOrderListFromService()
    {
        self.view.showLoader()
        let urlStr = HMLServiceUrls.getCompleteUrlFor(Url_Get_History_Order)+"/\(HMLUtlities.getUserContactNumber())"
        
        HMLAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            print(responseData)
            let JSON : NSDictionary = responseData as! NSDictionary
            let orderFromService = JSON["orders"] as? NSArray
            self.orderData = Order.getOrderFromJSON(orderFromService!)
            self.tableView.reloadData()
           
            
            }) { (errorMessage) -> () in
                print(errorMessage.localizedDescription)
                self.view.hideLoader()
                self.showErrorMessage(errorMessage.localizedDescription as String)
        }
    }
    func setNoDataView(){
        noDataView = HMLNoDataView.initNoDataView(self.view)
        noDataView.initWithDefaultValue("No past orders", description: "")
        self.tableView.backgroundView = noDataView
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

// MARK: - Table View Data Source & Delegate Method

func numberOfSectionsInTableView(tableView: UITableView) -> Int {
  // #warning Incomplete implementation, return the number of sections
  return 1
}

  func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }

  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }

func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if orderData.count == 0{
        
        noDataView.hidden = false
    }
    else{
        noDataView.hidden = true
    }

  return self.orderData.count
}

func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
{
  let order = self.orderData[indexPath.row] as! Order
  let cell = tableView.dequeueReusableCellWithIdentifier("pastOrderCell", forIndexPath: indexPath) as! HMLPastOrderTableViewCell
  cell.configureCell(indexPath, order: order)
  cell.layoutIfNeeded()
  return cell
}



func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
  print(indexPath.row)

  let orderDetailViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMLOrderDetailViewController") as? HMLOrderDetailViewController
  orderDetailViewController?.orderObj = self.orderData[indexPath.row]as! Order
  self.navigationController?.pushViewController(orderDetailViewController!, animated: true)
 }
}
