//
//  HMLSignInViewController.swift
//  Homli
//
//  Created by daffolapmac on 08/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLSignInViewController: HMLBaseViewController,UITextFieldDelegate {
    
    @IBOutlet var txtFldContact: HMLTextField!
    @IBOutlet var txtFldPassword: HMLTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        txtFldContact.addButtonOnLeftWithImageName("ic_signin_mobile")
        txtFldPassword.addButtonOnLeftWithImageName("icn_register_lock")
        setBackButton()
        self.title = "SIGN IN"
        self.txtFldContact.delegate = self
        HMLUtlities.setUserFromAddressScreen(false)
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = false
        
        
    }
    @IBAction func signInButtonAction(sender: AnyObject) {
        if(!HMLValidation.validateSignIn(self.txtFldContact.text!, password: self.txtFldPassword.text!))
        {
            return;
        }
        self.view.showLoader()
        let parameters = [
            "phoneNumber": self.txtFldContact.text!,
            "userPwd": self.txtFldPassword.text!,
        ]
        let url : String = HMLServiceUrls.getCompleteUrlFor(Url_LogIn)
        HMLAppServices.sharedInstance().postServiceRequest(urlString:url, parameter: parameters, successBlock: { (responseData) -> () in
            
            let response : NSDictionary = responseData as! NSDictionary
            self.view.hideLoader()
            print(response)
            let boolValue = response["loginSuccess"] as? Bool
            
            if boolValue == false
            {
                self.showErrorMessage(response["message"] as! String)
            }
            else{
                HMLUtlities.setUserInSignUpProcess(false)
                HMLUtlities.saveUserContactNumber(self.txtFldContact.text!)
                self.navigateToHomeViewController()
            }
            
            }) { (errorMessage) -> () in
                
                self.view.hideLoader()
                self.showErrorMessage(errorMessage.localizedDescription)
                
        }
        
    }
    //MARK:- UITextFieldDelegate Method
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= Character_Limit // Bool
    }
    @IBAction func helpButtonAction(sender: AnyObject) {
        
        let view =  NSBundle.mainBundle().loadNibNamed("HMLSignInHelpView", owner: self, options: nil)[0] as! UIView
        view.frame = self.view.frame
        self.view.addSubview(view)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
