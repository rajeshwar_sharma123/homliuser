//
//  HMLEditAccountViewController.swift
//  Homli
//
//  Created by Vishwas Singh on 3/18/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLEditAccountViewController: UIViewController {

	@IBOutlet var nameText: UITextField!
	@IBOutlet var idText: UITextField!
	@IBOutlet var numberText: UITextField!
    
	// MARK:LifeCycle method
	override func viewDidLoad()
	{
		super.viewDidLoad()
		numberText.userInteractionEnabled = false
		idText.userInteractionEnabled = false
		self.title = "EDIT ACCOUNT"

		nameText.text = HMLUtlities.getUserName()
		numberText.text = HMLUtlities.getUserContactNumber()
		// idText.text = HMLUtlities.getSignInUserDetail()["email"]

		let backButton = UIButton(type: UIButtonType.Custom)
		backButton.addTarget(self, action: #selector(HMLEditAccountViewController.didTapBackButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		// backButton.setTitle("TITLE", forState: UIControlState.Normal)
		backButton.setImage(UIImage(named: "ic_action_close"), forState: UIControlState.Normal)
		backButton.sizeToFit()
		let backButtonItem = UIBarButtonItem(customView: backButton)
		self.navigationItem.leftBarButtonItem = backButtonItem

		let rightButton = UIButton(type: UIButtonType.Custom)
		rightButton.addTarget(self, action: #selector(HMLEditAccountViewController.didTapDoneButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		// backButton.setTitle("TITLE", forState: UIControlState.Normal)
		rightButton.setImage(UIImage(named: "ic_done"), forState: UIControlState.Normal)
		rightButton.sizeToFit()
		let rightButtonItem = UIBarButtonItem(customView: rightButton)
		self.navigationItem.rightBarButtonItem = rightButtonItem
	}

	@IBAction func btnChangePassword(sender: AnyObject) {

		let view = NSBundle.mainBundle().loadNibNamed("HMLChangePasswordView", owner: self, options: nil)[0] as! HMLChangePasswordView
		view.frame = self.view.frame
		self.view.addSubview(view)

	}
    
    //MARK:This method return to Back Button
	func didTapBackButton(sender: AnyObject)
	{
		self.navigationController?.popViewControllerAnimated(true)
	}

	func didTapDoneButton(sender: AnyObject)
	{
		callServiceToUpdateProfile()

	}

	// MARK: Service Calling for Update Profile
	func callServiceToUpdateProfile() {
		if nameText.text?.trim().characters.count == 0 {
			self.showErrorMessage(ENTER_YOUR_NAME)
			return
		}
		self.view.showLoader()

		let parameters = [
			"mobileNo": HMLUtlities.getUserContactNumber(),
			"name": nameText.text!,
		]

		print(parameters)

		HMLAppServices.sharedInstance().postServiceRequest(urlString: HMLServiceUrls.getCompleteUrlFor(Url_Update_Account_Details), parameter: parameters, successBlock: { (responseData) -> () in

			let response: NSDictionary = responseData as! NSDictionary
			self.view.hideLoader()
			print(response)
			let boolValue = response["success"] as! Bool
			if boolValue {
				HMLUtlities.updateUserName(self.nameText.text!)
				self.showSuccessMessage("Profile name updated successfully")
				self.navigationController?.popViewControllerAnimated(true)

			}
			else {

			}

		}) { (errorMessage) -> () in

			self.view.hideLoader()
			self.showErrorMessage(errorMessage.localizedDescription)

		}

	}
}
