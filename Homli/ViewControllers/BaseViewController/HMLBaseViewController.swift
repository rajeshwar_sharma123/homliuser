//
//  HMLBaseViewController.swift
//  Homli
//
//  Created by daffolapmac on 08/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLBaseViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		setLeftMenuButton()
		// Do any additional setup after loading the view.
	}

	// *********** Method to set left menu button **********
	func setLeftMenuButton()
	{
		self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "img_nav_bar_7.png"), forBarMetrics: UIBarMetrics.Default);
		self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "top_nav_bg_ios7.png"), forBarMetrics: UIBarMetrics.Compact)

		let buttonImage = UIImage(named: "icn_menu.png");

		let leftBarButton: UIButton = UIButton(type: UIButtonType.Custom)
		leftBarButton.frame = CGRectMake(0, 0, 30, 30) ;
		leftBarButton.setImage(buttonImage, forState: UIControlState.Normal)
		leftBarButton.addTarget(self, action: #selector(HMLBaseViewController.leftMenuBtnClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)

		let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftBarButton)

		self.navigationItem.setLeftBarButtonItem(leftBarButtonItem, animated: false);
	}

	func leftMenuBtnClicked(sender: UIButton!)
	{
		self.toggleLeftSide()

	}

	func setBackButton()
	{
		let buttonImage = UIImage(named: "ic_action_close");
		let leftBarButton: UIButton = UIButton(type: UIButtonType.Custom)
		leftBarButton.frame = CGRectMake(0, 0, 40, 40) ;
		leftBarButton.setImage(buttonImage, forState: UIControlState.Normal)
		leftBarButton.addTarget(self, action: #selector(HMLBaseViewController.backBtnClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)

		let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftBarButton)

		self.navigationItem.setLeftBarButtonItem(leftBarButtonItem, animated: false);
	}

	func backBtnClicked(sender: UIButton!)
	{
		self.navigationController?.popViewControllerAnimated(true)

	}

}
