//
//  HMLMenuViewController.swift
//  Homli
//
//  Created by daffolapmac on 18/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLMenuViewController: HMLBaseViewController, UITableViewDataSource, UITableViewDelegate, HMLFilterViewDelegate, HMLTextFieldDelegate, UITextFieldDelegate {
	var noDataView: HMLNoDataView!
	@IBOutlet var btnAddLocation: UIButton!
	@IBOutlet var tableView: UITableView!
	var platesDetail = [HMLPlateDetailModel]()
	@IBOutlet var searchViewHeightConstraint: NSLayoutConstraint!
	@IBOutlet var cartViewConstraintConstant: NSLayoutConstraint!
	@IBOutlet var bottomCartView: UIView!
	@IBOutlet var lblTotalPrice: UILabel!
	@IBOutlet var btnGoToCart: UIButton!
	@IBOutlet var cartImgView: UIImageView!
	var searchBarBtn: UIButton!
	var filterView: HMLFilterView!
	var activityView = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
	@IBOutlet var txtFldSearch: HMLTextField!
	var veg: Bool = false
	var nonVeg: Bool = false
	var placeOrderModelObj = HMLPlaceOrderModel.sharedInstance()
	@IBOutlet var lblLocation: UILabel!
	@IBOutlet var addressView: UIView!
	var userDetailsDic: NSMutableDictionary = [:]

	// MARK:LifeCycle method
	override func viewDidLoad() {

		super.viewDidLoad()
		// Do any additional setup after loading the view.
		self.title = "MENU"
		self.automaticallyAdjustsScrollViewInsets = false
		setNoDataView()
		self.tableView.registerNib(UINib(nibName: "HMLMenuCell", bundle: nil), forCellReuseIdentifier: "HMLMenuCell")
		setRightBarButton()
		self.btnAddLocation.setImageColorFromHexaCodeForNormalState(0xF9A331)
		btnGoToCart.layer.cornerRadius = 4
		lblTotalPrice.text = String(0).setIndianCurrencyStyle()
		cartImgView.setBadgeHidden(false)
		cartImgView.setBadgeBackgroundColor(ColorFromHexaCode(k_Default_Orange_Color))
		cartImgView.setBadgeFontColor(UIFont.systemFontOfSize(10))
		cartImgView.setBadgeStyle(BadgeStyle.Number(9))
		searchViewHeightConstraint.constant = 0
		txtFldSearch.layer.borderWidth = 2
		txtFldSearch.layer.borderColor = ColorFromHexaCode(k_Default_Orange_Color).CGColor
		txtFldSearch.addButtonOnRightWithImageName("ic_close")
		txtFldSearch.delegateObj = self
		activityView.frame = CGRectMake((screenWidth - 30) / 2, CGRectGetHeight(addressView.frame), 30, 30)
		activityView.color = ColorFromHexaCode(k_Default_Orange_Color)
		self.view.addSubview(activityView)

	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		setSideMenuPanModeDefault()
		userDetailsDic = HMLUtlities.getSignInUserDetail()
		let userAddress = userDetailsDic["address"]!
		var deliveryAddress = HMLPlaceOrderModel.sharedInstance().deliveryAddress
		if deliveryAddress == nil {
			deliveryAddress = userAddress as? NSDictionary
		}
		txtFldSearch.text = ""
		getPlateListFromService("")
		lblLocation.text = "\(deliveryAddress!["addrField1"]!) \(deliveryAddress!["addrField2"]!)"

		lblTotalPrice.text = String(HMLPlaceOrderModel.sharedInstance().totalAmount).setIndianCurrencyStyle()
		if placeOrderModelObj.orderItems.count == 0 {
			self.cartViewConstraintConstant.constant = 0
		}
		else {
			self.cartViewConstraintConstant.constant = 60
		}
		cartImgView.setNumberBadge(UInt(placeOrderModelObj.orderItems.count))
		tableView.reloadData()

	}
	func setNoDataView() {
		noDataView = HMLNoDataView.initNoDataView(self.view)
		noDataView.lblTitle.textColor = UIColor.lightGrayColor()
		noDataView.initWithDefaultValue("We can not seem to locate your choice.", description: "")
		self.tableView.backgroundView = noDataView

	}
	func getLatLongLocation(str: String) -> String {
		userDetailsDic = HMLUtlities.getSignInUserDetail()
		let userAddress = userDetailsDic["address"]!
		var deliveryAddress = HMLPlaceOrderModel.sharedInstance().deliveryAddress
		if deliveryAddress == nil {
			deliveryAddress = userAddress as? NSDictionary
		}
		if str == "latitude" {
			return deliveryAddress!["latitude"] as! String
		}
		else {
			return deliveryAddress!["longitude"] as! String
		}

	}
	func setActivityIndicatorPosition() {
		activityView.frame = CGRectMake((screenWidth - 30) / 2, CGRectGetMaxY(addressView.frame), 30, 30)
	}
	/**
	 Service Calling of getting the Plate List

	 - parameter plateName: <#plateName description#>
	 */
	func getPlateListFromService(plateName: String)
	{
		setActivityIndicatorPosition()
		let parameters = [
			"plateName": plateName,
			"lattitude": getLatLongLocation("latitude"),
			"longitude": getLatLongLocation("longitude"),
			"veg": veg,
			"nonVeg": nonVeg,
			"subCuisineTypes": []

		]
		activityView.startAnimating()
		print(parameters)
		HMLAppServices.sharedInstance().postServiceRequest(urlString: HMLServiceUrls.getCompleteUrlFor(Url_Menu_Plate_Search), parameter: parameters, successBlock: { (responseData) -> () in
			self.activityView.stopAnimating()
			let response: NSDictionary = responseData as! NSDictionary
			print(response)
			self.platesDetail = HMLPlateDetailModel.bindDataWithModel(response["plates"] as! NSArray)
			self.tableView.reloadData()

		}) { (errorMessage) -> () in

			self.activityView.stopAnimating()
			print(errorMessage)

		}

	}
	// MARK: - Table View Data Source & Delegate Method
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return 1
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// return 10

		if platesDetail.count == 0 {

			noDataView.hidden = false
		}
		else {
			noDataView.hidden = true
		}

		return platesDetail.count
	}
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

		return UITableViewAutomaticDimension
	}
	func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

		return UITableViewAutomaticDimension
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
	{

		let cell = tableView.dequeueReusableCellWithIdentifier("HMLMenuCell", forIndexPath: indexPath) as! HMLMenuCell
		configureCell(cell, atIndexPath: indexPath)
		cell.layoutIfNeeded()

		return cell
	}

	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

	}

	func configureCell(cell: HMLMenuCell, atIndexPath indexPath: NSIndexPath)
	{
		let plate = self.platesDetail[indexPath.row] as HMLPlateDetailModel
		cell.lblPlateName.text = plate.plateName
		cell.lblPlateDescription.text = plate.plateDescription
		cell.ratingStarView.rating = Double(plate.rating!)!
		cell.lblPrice.text = plate.price!.setIndianCurrencyStyle()
		cell.lblPlateAvailable.text = "Only " + plate.availablePlates! + " plates left"

		cell.btnRemove.addTarget(self, action: #selector(HMLMenuViewController.removeButtonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		cell.btnRemove.setImageColorFromHexaCodeForNormalState(0xF9A331)
		cell.btnCart.addTarget(self, action: #selector(HMLMenuViewController.cartButtonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		cell.btnCart.setImageColorFromHexaCodeForNormalState(0xF9A331)
		cell.btnAdd.addTarget(self, action: #selector(HMLMenuViewController.addButtonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		cell.btnAdd.setImageColorFromHexaCodeForNormalState(0xF9A331)
		cell.tag = indexPath.row
		let plateTmp = placeOrderModelObj.getPlateIfExist(plate)
		if plateTmp.noOfPlates != 0 {
			cell.quantityView.hidden = false
			cell.cartView.hidden = true
			cell.lblCount.text = String(plateTmp.noOfPlates)
		}
		else {
			cell.cartView.hidden = false
			cell.quantityView.hidden = true
			cell.lblCount.text = "1"
		}

	}

	func addButtonClicked(sender: UIButton)
	{
		let cell: HMLMenuCell = sender.superview!.superview!.superview!.superview as! HMLMenuCell
		let plate = self.platesDetail[cell.tag] as HMLPlateDetailModel
		var count = Int (cell.lblCount.text!)
		let plateLeft = Int(plate.availablePlates!)
		if count < plateLeft {
			cell.lblCount.text = String(count! += 1)
		}
		else {
			return
		}
		setTotalPrice(cell.tag)
		let plateTmp = placeOrderModelObj.getPlateIfExist(plate)
		if plateTmp.noOfPlates != 0 {
			plateTmp.noOfPlates = count!
		}

	}

	/**
	 This function set the Total Price of order
	 */
	func setTotalPrice(index: Int) {

		let plate = self.platesDetail[index] as HMLPlateDetailModel
		let price1 = lblTotalPrice.text!.convertToDoubleValue()
		let price2 = Double(plate.price!)
		lblTotalPrice.text = String(price1 + price2!).setIndianCurrencyStyle()
		placeOrderModelObj.totalAmount = lblTotalPrice.text!.convertToDoubleValue()
	}
	/**
	 This fuction set the Deduction Price
	 */
	func deductTotalPrice(index: Int) {

		let plate = self.platesDetail[index] as HMLPlateDetailModel
		let price1 = lblTotalPrice.text!.convertToDoubleValue()
		let price2 = Double(plate.price!)
		lblTotalPrice.text = String(price1 - price2!).setIndianCurrencyStyle()
		placeOrderModelObj.totalAmount = lblTotalPrice.text!.convertToDoubleValue()
	}
	func removeButtonClicked(sender: UIButton)
	{

		let cell: HMLMenuCell = sender.superview!.superview!.superview!.superview as! HMLMenuCell
		deductTotalPrice(cell.tag)
		let plate = self.platesDetail[cell.tag] as HMLPlateDetailModel
		var count = Int (cell.lblCount.text!)

		if (count == 1)
		{
			cell.cartView.frame = CGRectMake(91, cell.cartView.frame.origin.y, cell.cartView.frame.size.width, cell.cartView.frame.size.height)
			UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
				cell.cartView.frame = CGRectMake(63, cell.cartView.frame.origin.y, cell.cartView.frame.size.width, cell.cartView.frame.size.height)
				cell.cartView.hidden = false
				cell.quantityView.frame = CGRectMake(-91, cell.quantityView.frame.origin.y, cell.quantityView.frame.size.width, cell.quantityView.frame.size.height)

				count = 0
				}, completion: { finished in
				cell.quantityView.hidden = true

			})

		}
		else {

			cell.lblCount.text = String(count! -= 1)
		}
		if (count == 0)
		{
			for i in 0..<placeOrderModelObj.orderItems.count {
				let plateTmp = placeOrderModelObj.orderItems[i]
				if plate.plateCode == plateTmp.plateCode {
					placeOrderModelObj.orderItems.removeAtIndex(i)
				}

			}
			cartImgView.setNumberBadge(UInt(placeOrderModelObj.orderItems.count))
		}
		else
		{
			let plateTmp = placeOrderModelObj.getPlateIfExist(plate)
			if plateTmp.noOfPlates != 0 {
				plateTmp.noOfPlates = count!
			}

		}

		if placeOrderModelObj.orderItems.count == 0 {
			self.cartViewConstraintConstant.constant = 0
		}

	}

	/**
	 This method is set the Price
	 */
	func cartButtonClicked(sender: UIButton)
	{
		let cell: HMLMenuCell = sender.superview!.superview!.superview!.superview as! HMLMenuCell
		setTotalPrice(cell.tag)
		let plate = self.platesDetail[cell.tag] as HMLPlateDetailModel
		plate.noOfPlates = 1
		placeOrderModelObj.orderItems.append(self.platesDetail[cell.tag] as HMLPlateDetailModel)
		cartImgView.setNumberBadge(UInt(placeOrderModelObj.orderItems.count))

		cell.quantityView.frame = CGRectMake(91, cell.quantityView.frame.origin.y, cell.quantityView.frame.size.width, cell.quantityView.frame.size.height)

		UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
			cell.cartView.frame = CGRectMake(-91, cell.cartView.frame.origin.y, cell.cartView.frame.size.width, cell.cartView.frame.size.height)
			cell.quantityView.hidden = false
			cell.quantityView.frame = CGRectMake(0, cell.quantityView.frame.origin.y, cell.quantityView.frame.size.width, cell.quantityView.frame.size.height)
			self.cartViewConstraintConstant.constant = 60

			}, completion: { finished in
			cell.cartView.hidden = true
		})
	}

	@IBAction func goToCartButtonAction(sender: AnyObject) {

		print("cart button tapped: \(placeOrderModelObj.orderItems)")

		let vc = self.storyboard!.instantiateViewControllerWithIdentifier("HMLYourCartViewController") as? HMLYourCartViewController
		self.navigationController?.pushViewController(vc!, animated: true)

	}
	func setRightBarButton()
	{

		var buttonImage = UIImage(named: "ic_menu_search");

		let filterBarBtn: UIButton = UIButton(type: UIButtonType.Custom)
		filterBarBtn.frame = CGRectMake(0, 0, 40, 30) ;
		filterBarBtn.setImage(buttonImage, forState: UIControlState.Normal)
		filterBarBtn.addTarget(self, action: #selector(HMLMenuViewController.filterBarBtnClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		let filterBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: filterBarBtn)

		buttonImage = UIImage(named: "ic_menu_filter");
		searchBarBtn = UIButton(type: UIButtonType.Custom)
		searchBarBtn.frame = CGRectMake(0, 0, 30, 30) ;
		searchBarBtn.setImage(buttonImage, forState: UIControlState.Normal)
		searchBarBtn.addTarget(self, action: #selector(HMLMenuViewController.searchBarBtnClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
		let searcBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: searchBarBtn)
		searchBarBtn.tag = 0

		self.navigationItem.setRightBarButtonItems([filterBarButtonItem, searcBarButtonItem], animated: false)
	}

	func filterBarBtnClicked(sender: UIButton!)
	{

		if filterView == nil {
			filterView = HMLFilterView.addFilterView(self.view)
			filterView.delegate = self
		}
		filterView.toggelView()

	}
	func searchBarBtnClicked(sender: UIButton!)
	{

		if sender.tag == 0 {
			sender.tag = 1
			searchViewHeightConstraint.constant = 0
			self.view.layoutIfNeeded()
			UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
				self.searchViewHeightConstraint.constant = 50
				self.view.layoutIfNeeded()

			}) { _ in

			}

		}
		else if sender.tag == 1 {
			sender.tag = 0
			self.view.layoutIfNeeded()
			UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
				self.searchViewHeightConstraint.constant = 0
				self.view.layoutIfNeeded()

			}) { _ in

			}

		}
		print("search")

	}

	@IBAction func addLocationButtonAction(sender: AnyObject) {
		let vc = self.storyboard?.instantiateViewControllerWithIdentifier("HMLAddressesViewController") as! HMLAddressesViewController
		self.navigationController?.pushViewController(vc, animated: true)

	}
	// MARK:- HMLFilterViewDelegate Delegate
	func getPreferencesToFilterItem(veg: Bool, nonVeg: Bool) {
		self.veg = veg
		self.nonVeg = nonVeg
		getPlateListFromService("")
	}
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	// MARK:- HMLTextFieldDelegate Method
	func rightButtonAction() {

		if txtFldSearch.text?.characters.count != 0 {
			txtFldSearch.text = ""
			return
		}

		searchBarBtn.tag = 0
		self.view.layoutIfNeeded()
		UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
			self.searchViewHeightConstraint.constant = 0
			self.view.layoutIfNeeded()

		}) { _ in

		}

	}

	// MARK:- UITextFieldDelegate Method
	func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
		var dishName: NSString = textField.text! as NSString
		dishName = dishName.stringByReplacingCharactersInRange(range, withString: string)
		getPlateListFromService(dishName as String)
		return true

	}
}
