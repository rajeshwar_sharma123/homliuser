//
//  HMLContactUsViewController.swift
//  Homli
//
//  Created by Vishwas Singh on 3/18/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLContactUsViewController: HMLBaseViewController, UITextViewDelegate {


  @IBOutlet var msgTxtView: UITextView!

  override func viewDidLoad() {
    super.viewDidLoad()
    setBackButton()
    self.title = "CONTACT US"
    msgTxtView.layer.cornerRadius = 25
    msgTxtView.clipsToBounds = true
    msgTxtView.delegate = self
    msgTxtView.textContainerInset = UIEdgeInsets(top: 8, left: 20, bottom: 8, right: 20)
    msgTxtView.text = "Message"
    msgTxtView.textColor = UIColor.lightGrayColor()
    setSideMenuPanModeNone()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func btnSubmit(sender: AnyObject) {

    if self.msgTxtView.text.trim().characters.count > 0 && self.msgTxtView.text != "Message"
    {
      self.callServiceForQuery()
    }
    else{
      self.showErrorMessage("Enter your message")
    }
  }

  func textViewDidBeginEditing(textView: UITextView) {
    if textView.textColor == UIColor.lightGrayColor() {
      textView.text = nil
      textView.textColor = UIColor.blackColor()
    }
  }

  func textViewDidEndEditing(textView: UITextView) {
    if textView.text.isEmpty {
      textView.text = "Message"
      textView.textColor = UIColor.lightGrayColor()
    }
  }

  func callServiceForQuery()
  {
    let signInUserDic = HMLUtlities.getSignInUserDetail()
    print(signInUserDic)


    let parameters = [
      "mobileNumber": signInUserDic["mobileNumber"]!,
      "message": self.msgTxtView.text!,
      "emailId": signInUserDic["email"]!,
    ]

    let url : String = HMLServiceUrls.getCompleteUrlFor(Url_Contact_Us)

    self.view.showLoader()
    HMLAppServices.sharedInstance().postServiceRequest(urlString:url, parameter: parameters, successBlock: { (responseData) -> () in

      let response : NSDictionary = responseData as! NSDictionary
      self.view.hideLoader()
      print(response)
      let boolValue = response["createSuccess"] as? Bool

      if boolValue == true
      {
        self.showSuccessMessage("Server Error")
        self.navigateToHomeViewController()
      }
      else{
        self.showErrorMessage(response["message"] as! String)
      }



      }) { (errorMessage) -> () in
        
        self.view.hideLoader()
        self.showErrorMessage(errorMessage.localizedDescription)
        
    }
  }
  
}
