//
//  HMLUtlities.swift
//  Homli
//
//  Created by daffolapmac on 11/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import MFSideMenu
class HMLUtlities: NSObject {

	/**
	 This Method is save the USerProfile
	 */
	class func saveUserProfile(userProfile: NSDictionary)
	{

		let userProf = userProfile.removeAllNullValues()
		print(userProf)
		if userProf.count == 0 {
			return
		}
		var tmpDic = userProf["address"]?.removeAllNullValues()
		print(tmpDic)
		if tmpDic?.count == 0 {

			userProf.removeObjectForKey("address")
		}
		else
		{
			userProf.setObject(tmpDic!, forKey: "address")
		}

		print(userProf)
		tmpDic = userProf["preferences"]?.removeAllNullValues()
		if tmpDic != nil {
			userProf.setObject(tmpDic!, forKey: "preferences")
		}

		print(userProf)
		UserDefault.setObject(userProf, forKey: UserProfile)

		UserDefault.synchronize()

	}

	/**
	 This Method is for save the UserToken
	 */
	class func saveUserToken(token: String)
	{
		UserDefault.setObject(token, forKey: Token)
		UserDefault.synchronize()
	}

	/**
	 This metho is save UserContactNumber
	 */
	class func saveUserContactNumber(contact: String)
	{
		UserDefault.setObject(contact, forKey: ContactNumber)
		UserDefault.synchronize()
	}

	/**
	 This method is save the SignUpUserDetail
	 */
	class func saveSignUpUserDetail(detail: NSMutableDictionary)
	{
		let response = detail.removeAllNullValues()
		UserDefault.setObject(response, forKey: SignUpUser)
		UserDefault.synchronize()
	}

	/**
	 This method is set the User For SignIn
	 */
	class func setUserInSignUpProcess(state: Bool)
	{
		UserDefault.setObject(state, forKey: User_Flow)
		UserDefault.synchronize()

	}

	/**
	 This method save status UserOTP
	 */
	class func setUserOTPVerifiedStatus(state: Bool)
	{
		UserDefault.setObject(state, forKey: User_OTP_Status)
		UserDefault.synchronize()

	}

	/**
	 This method is set status for UserAddressDetail
	 */
	class func setUserAddressDetailStatus(state: Bool)
	{
		UserDefault.setObject(state, forKey: User_Address_Detail)
		UserDefault.synchronize()

	}

	/**
	 This method is set the User for AddressScreen
	 */
	class func setUserFromAddressScreen(state: Bool)
	{
		UserDefault.setObject(state, forKey: User_Address_Screen)
		UserDefault.synchronize()

	}

	/**
	 This method is validate the User From Address Screen
	 */
	class func isUserFromAddressScreen() -> Bool
	{
		if UserDefault.objectForKey(User_Address_Screen) != nil {
			return UserDefault.objectForKey(User_Address_Screen) as! Bool
		}
		return false
	}

	/**
	 This method chech the User is SignInProcess
	 */
	class func isUserInSignUpProcess() -> Bool
	{
		if UserDefault.objectForKey(User_Flow) != nil {
			return UserDefault.objectForKey(User_Flow) as! Bool
		}
		return false
	}

	/**
	 This method is Verified the UserOTP
	 */
	class func isUserOTPVerified() -> Bool
	{
		if UserDefault.objectForKey(User_OTP_Status) != nil {
			return UserDefault.objectForKey(User_OTP_Status) as! Bool
		}
		return false

	}

	/**
	 This method Check the User AddressDetail Filled
	 */
	class func isUserAddressDetailFilled() -> Bool
	{
		if UserDefault.objectForKey(User_Address_Detail) != nil {
			return UserDefault.objectForKey(User_Address_Detail) as! Bool
		}
		return false

	}

	/**
	 This method check the User Address Available
	 */
	class func isUserAddressAvailable() -> Bool
	{
		if HMLUtlities.isUserLogedIn() {
			let userDetail = UserDefault.objectForKey(UserProfile) as! NSMutableDictionary
			if userDetail.objectForKey(User_Address) != nil {
				return true
			}
		}

		return false
	}

	/**
	 This method get SignInUserDetail
	 */
	class func getSignInUserDetail() -> NSMutableDictionary
	{

		if HMLUtlities.isUserLogedIn() {
			return UserDefault.objectForKey(UserProfile) as! NSMutableDictionary

		}
		return [:]
	}

	/**
	 This method get the UserName
	 */
	class func getUserName() -> String
	{
		let userInfo = HMLUtlities.getSignInUserDetail()
		if let name = userInfo["name"] {
			return name as! String
		}
		return ""

	}

	/**
	 This method get the UserEmailId
	 */
	class func getUserEmailId() -> String
	{
		let userInfo = HMLUtlities.getSignInUserDetail()
		if let email = userInfo["email"] {
			return email as! String
		}
		return ""

	}

	/**
	 This method Update the UserName
	 */
	class func updateUserName(name: String) {
		if HMLUtlities.isUserLogedIn() {
			let userInfo = HMLUtlities.getSignInUserDetail().mutableCopy() as! NSMutableDictionary
			userInfo.setObject(name, forKey: "name")
			HMLUtlities.saveUserProfile(userInfo)

		}
	}

	/**
	 This method get the UserContactNumber
	 */
	class func getUserContactNumber() -> String
	{

		return UserDefault.objectForKey(ContactNumber) as! String

	}

	/**
	 This method get the the SignUpUserDetail
	 */
	class func getSignUpUserDetail() -> NSMutableDictionary
	{
		if UserDefault.objectForKey(SignUpUser) != nil {
			return UserDefault.objectForKey(SignUpUser) as! NSMutableDictionary
		}
		return [:]

	}

	class func getOTP() -> String
	{
		let dic: NSMutableDictionary = UserDefault.objectForKey(SignUpUser) as! NSMutableDictionary
		return dic["otp"] as! String

	}

	/**
	 This method set the navigation
	 */

	class func getTopViewController() -> UIViewController
	{

		let topController = UIApplication.sharedApplication().keyWindow?.rootViewController
		var vc = UIViewController()
		if ((topController?.isKindOfClass(MFSideMenuContainerViewController)) != nil) {

			let sideMenuVC: MFSideMenuContainerViewController = topController as! MFSideMenuContainerViewController
			let navController = sideMenuVC.centerViewController
			vc = navController.topViewController!!

		}
		return vc
	}

	/**
	 This method get the navigationController
	 */
	func getCurrentNavigationController() -> UINavigationController
	{
		let navigationController = UIApplication.sharedApplication().keyWindow?.rootViewController?.navigationController
		return navigationController!
	}

	/**
	 This method check the UserLogged
	 */
	class func isUserLogedIn() -> Bool
	{
		if UserDefault.objectForKey(UserProfile) != nil {
			return true
		}
		return false
	}

	/**
	 This method SignOut from App
	 */
	class func signOutFromApp()
	{
		HMLUtlities.setUserInSignUpProcess(false)
		UserDefault.removeObjectForKey(UserProfile)

	}

	/**
	 This method Show the Error Message
	 */
	class func showErrorMessage(message: String)
	{
		let vc: UIViewController = HMLUtlities.getTopViewController()
		vc.showErrorMessage(message)
	}

	/**
	 This method show the Success Message
	 */
	class func showSuccessMessage(message: String)
	{
		let vc: UIViewController = HMLUtlities.getTopViewController()
		vc.showSuccessMessage(message)
	}

}
