//
//  HMLConfig.swift
//  Homli
//
//  Created by daffolapmac on 11/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//
import Foundation
import UIKit


// ************************************* Url Constants ****************************************

//**************** Base Url for live *********************************
/*let Base_Url = "http://ec2-54-149-185-211.us-west-2.compute.amazonaws.com:9080/v1/user"

let Url_LogIn                  = "/login"
let Url_Register               = "/register"
let Url_Read_User_Detail       = "/read"
let Url_Confirm_OTP            = "/confirmotp"
let Url_First_Detail           = "/firstDetails"
let Url_Request_OTP            = "/requestotp"
let Url_Menu_Plate_Search      = "/menu/plateSearch"

//======= Order Url ==========
let Url_Get_Order_List         = "/order/getOrders"
let Url_Get_Order_Detail      = "/order/getOrderDetails"*/


//**************** Base Url for Dev *********************************
let Base_Url = "http://ec2-54-254-134-129.ap-southeast-1.compute.amazonaws.com:9080/v1/user"

let Url_LogIn                  = "/login"
let Url_Register               = "/register"
let Url_Read_User_Detail       = "/read"
let Url_Confirm_OTP            = "/confirmotp"
let Url_First_Detail           = "/firstDetails"
let Url_Request_OTP            = "/requestotp"
let Url_Menu_Plate_Search      = "/menu/plateSearch"
let Url_Contact_Us             = "/query"
let Url_ChangePassword         = "/changePassword"
let Url_Get_Address_List       = "/addresses"
let Url_To_Add_Address         = "/addAddress"
let Url_To_Delete_Address      = "/deleteAddress"
let Url_To_Update_Primary      = "/updateprimary"
let Url_Update_Account_Details = "/updateAccountDetails"


//======= Order Url ==========
let Url_Get_Order_List         = "/order/getOrders"
let Url_Get_Order_Detail       = "/order/getOrderDetails"
let Url_Get_Promo_code         = "/order/applyPromoCode"
let Url_To_Place_Order         = "/order/placeOrder"
let Url_To_Cancel_Order        = "/order/cancelOrder"
let Url_payment_Method         = "/deliverySlots"
let Url_Create_Order           = "/order/createOrder"
let Url_Update_Payment_RefId   = "/order/updatePaymentRefId"
let Url_Change_Password        = "/changePassword"
let Url_Get_History_Order      = "/order/getHistoryOrders"

// *******************************  Basic Config Constants ****************************

let Google_API_Key = "AIzaSyDXS55kNjIe34xsnyHiG1mklvFtSr0VO0g"
let PUBLIC_KEY_FOR_RAZOR_PAY   = "rzp_test_jlViJwgZxPvM31"


let Url_For_Default_Icon = "https://s3-ap-southeast-1.amazonaws.com/homli-chef-images/app_icon.jpg"
