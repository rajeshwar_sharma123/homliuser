//
//  HMLEnum.swift
//  Homli
//
//  Created by daffolapmac on 05/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation

enum PaymentMethod: String {
  case ONLINE = "ONLINE"
  case COD = "COD"
}


enum OrderStatus: String {
  case ASSIGNED = "Chef Assigned"
  case CANCELLED = "Cancelled"
  case CREATED = "Order Placed"
  case DELIVERED = "Delivered"
  case PARTIALLY_ASSIGNED = "Chef Partially Assigned"
  case PARTIALLY_CANCELLED = "Partially Cancelled"
  case PARTIALLY_DELIVERED = "Partially Delivered"
  case DELIVERY_DRIVER_ASSOCIATED = "Driver Assigned"
  case DELIVERY_PICKED_UP = "Delivery Picked Up"
  case SUCCESS  = "Success"
  case FAILED  = "Failed"
}
