//
//  HMLColorConstant.swift
//  Homli
//
//  Created by daffolapmac on 11/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation

func ColorFromHexaCode(rgbValue: UInt) -> UIColor {
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
//************************ Hexa Color Code ***************
let k_Theme_Color                 = 0xF49E2F as UInt
let k_Side_Menu_Color             = 0x1F2322 as UInt
let k_Side_Menu_Default_Color     = 0xA4A4A4 as UInt
let k_Side_Menu_InActive          = 0x424342 as UInt
let k_Header_Color                = 0x333333 as UInt
let k_Border_Color                = 0xEFCC79 as UInt
let k_Default_Orange_Color        = 0xF7AA4E as UInt
let k_Light_Gray_Color            = 0x9E9E9E as UInt

