//
//  HMLConstants.swift
//  Homli
//
//  Created by daffolapmac on 11/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
import UIKit

// ***********************************  Basic String Constants ****************************************

let UserDefault                  = NSUserDefaults.standardUserDefaults()
let Token                        = "Token"
let UserProfile                  = "userProfile"
let ContactNumber                = "contactNumber"

let SignUpUser                   = "signUpUser"
let App_Delegate                 = UIApplication.sharedApplication().delegate as! AppDelegate

let Character_Limit              = 10
let User_Flow                    = "UserFlow"
let User_OTP_Status              = "OTPStatus"
let User_Address_Detail          = "AddressDetail"
let User_Address                 = "address"
let User_Address_Screen          = "addressScreen"

// ************************************* Validation messages ****************************************

let ENTER_CONTACT_NUMBER             = "Please enter contact number"
let ENTER_VALID_CONTACT_NUMBER       = "Please enter valid contact number"
let ENTER_PASSWORD                   = "Please enter password"
let ENTER_NAME                       = "Please enter your Full Name"
let ENTER_EMAIL_ID                   = "Please enter email id"
let ENTER_VALID_EMAIL_ID             = "Please enter valid email id"
let ENTER_OTP                        = "Please enter OTP"
let ENTER_FLAT_BUILDING_NAME         = "Please provide Flat No / Building Name"
let ENTER_ADDRESS                    = "Please enter address"
let ENTER_LOCALITY                   = "Please select locality"
let ENTER_LANDMARK                   = "Please enter landmark"
let ENTER_POSTAL_CODE                = "Please enter postal code"
let ENTER_VALID_POSTAL_CODE          = "Postal Code must be 6 digits"
let ENTER_PIN_CODE                   = "Please enter pin code"
let ENTER_VALID_PIN_CODE             = "Pin Code must be 6 digits"
let ENTER_PREFERENCE                 = "Please select preferences"
let ENTER_YOUR_NAME                  = "Please enter your name"
let ENTER_OLD_PASSWORD               = "Please enter old password"
let ENTER_NEW_PASSWORD               = "Please enter new password"
let ENTER_ADDRESS_FROM_MAP           = "Please pick address from Map"
// **********************************  Alert message  ************************
let Order_placed_successful          = "Your order has been placed successfully"
let Order_placed_failed              = "Your order could not be placed due payment not completed"

/*ContactUs */
let Query_successful                 = "Your query submitted successfully."

// **********************************  Check for Device ************************


let screenWidth = UIScreen.mainScreen().bounds.width
let screenHeight = UIScreen.mainScreen().bounds.height

