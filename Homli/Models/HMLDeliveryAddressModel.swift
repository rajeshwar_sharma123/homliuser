//
//  HMLDeliveryAddressModel.swift
//  Homli
//
//  Created by daffolapmac on 16/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLDeliveryAddressModel: NSObject {

    var flat_building_name          : String!
    var address_line_1              : String!
    var address_line_2              : String!
    var locality                    : String!
    var landmark                    : String!
    var postal_code                 : String!
    var  tmpDeliveryAddrss : HMLDeliveryAddressModel!
    static let instance = HMLDeliveryAddressModel()
    override init() {
        super.init()
        setInitialValue()
    }
    class func sharedInstance() -> HMLDeliveryAddressModel
    {
        return instance;
    }
    
    func setInitialValue()
    {
        self.flat_building_name           = ""
        self.address_line_1               = ""
        self.address_line_2               = ""
        self.locality                     = ""
        self.landmark                     = ""
        self.postal_code                  = ""
    }
}

