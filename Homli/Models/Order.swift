//
//  Order.swift
//  Homli
//
//  Created by Jenkins on 3/15/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
class Order
{
    var orderId: String?
    var orderItems = []
    var orderDate: String?
    var status: String?
    
   
    //MARK: get data from Service
    /**
    - Parameter sender: Reference of order which is class
    */
    class func getOrderFromJSON(orderFromService: NSArray) -> [Order]
    {
        var orderData = [Order]()
        
        for i in 0..<orderFromService.count{
            let newOrder = Order()
            if let order_obj = orderFromService[i] as? NSDictionary{
                newOrder.orderId = order_obj["orderId"] as? String
                let items = order_obj["orderItems"] as? NSArray
                newOrder.orderItems = (items) as! [OrderItems]
                newOrder.orderDate = order_obj["orderDate"] as? String
                newOrder.status = order_obj["status"] as? String
                orderData.append(newOrder)
            }
        }
        return orderData
    }

  func isOrderCancellable(isCancelable: String) -> Bool
  {
    switch (isCancelable)
    {
    case OrderStatus.ASSIGNED.rawValue:
      return true

    case OrderStatus.CREATED.rawValue:
      return true

    case OrderStatus.PARTIALLY_ASSIGNED.rawValue:
      return true

    case OrderStatus.DELIVERY_DRIVER_ASSOCIATED.rawValue:
      return true
      
    default:
      return false
    }
  }
}
