//
//  HMLPlateDetailModel.swift
//  Homli
//
//  Created by daffolapmac on 18/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
class HMLPlateDetailModel {
    var plateName          : String?
    var plateDescription   : String?
    var plateCode          : String?
    var availablePlates    : String?
    var price              : String?
    var rating             : String?
    var noOfPlates         : Int = 0
    var index              : Int = 0

    //MARK: Custome Method

    class func bindDataWithModel(plates: NSArray) -> [HMLPlateDetailModel]
    {
        var platesArray = [HMLPlateDetailModel]()
        
        for i in 0..<plates.count{
            let plate = HMLPlateDetailModel()
            if let plateObj = plates[i] as? NSDictionary{
                print(plateObj)
                plate.plateName = plateObj["plateName"] as? String
                plate.plateDescription = plateObj["plateDescription"] as? String
                plate.plateCode = plateObj["plateCode"] as? String
//                plate.availablePlates = String(plateObj["availablePlates"] as! Int)
//                plate.price = String(plateObj["price"] as! Int)
//                plate.rating =  String(plateObj["rating"] as! Int)
                plate.availablePlates = String(plateObj["availablePlates"]!)
                plate.price = String(plateObj["price"]! )
                plate.rating =  String(plateObj["rating"]! )
                
                platesArray.append(plate)
                
            }
        }
        return platesArray
    }

  class func getSelectedPlates(plates: NSArray, selectedPlates: AnyObject) -> [HMLPlateDetailModel]
  {
    var tempPlates = [HMLPlateDetailModel]()
    let tempSelected:[Int:Int] = selectedPlates as! [Int : Int]

    for (key, value) in tempSelected {
      print("Dictionary key \(key) -  Dictionary value \(value)")

      //let plateCount = tempSelected[key];
      //var index = keys[atIndex] as Int
      let tempPlate = plates[key] as! HMLPlateDetailModel
       tempPlate.noOfPlates = value
       tempPlate.index = key

       tempPlates.append(tempPlate)
    }

    return tempPlates
  }
}