//
//  HMLOrderDetailModel.swift
//  Homli
//
//  Created by daffolapmac on 22/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
class HMLOrderDetailModel {
    var orderId                 : String?
    var status                  : String?
    var statusDate              : String?
    var rating                  : String?
    var address                 : NSDictionary?
    var orderItems              = []
    var events                  = []
    var receipt                 = HMLOrderReceiptModel()
    
    class func bindDataWithModel(order: NSDictionary) -> [HMLOrderDetailModel]
    {
        let orderStatus = HMLOrderDetailModel()
        var arr = [HMLOrderDetailModel]()
        orderStatus.orderId = order["orderId"] as? String
        orderStatus.status = order["status"] as? String
        orderStatus.statusDate = order["statusDate"] as? String
        orderStatus.rating = order["rating"] as? String
        orderStatus.address = order["address"] as? NSDictionary
        orderStatus.orderItems = (order["orderItems"] as? [HMLOrderItemModel])!
        orderStatus.events = HMLOrderEventsModel.bindDataWithModel((order["events"] as? NSArray)!)
        orderStatus.receipt = HMLOrderReceiptModel.bindDataWithModel((order["receipt"] as? NSDictionary)!)
        arr.append(orderStatus)
        return arr
    }
    
}
