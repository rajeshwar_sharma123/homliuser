//
//  HMLOrderEventsModel.swift
//  Homli
//
//  Created by daffolapmac on 22/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
class HMLOrderEventsModel {
    
    var eventDateTime            : String?
    var eventStatus              : String?
    var orderId                  : String?
    var statusMessage            : String?
    
    class func bindDataWithModel(events: NSArray) -> [HMLOrderEventsModel]
    {
        
        var eventsArr = [HMLOrderEventsModel]()
        
        for i in 0..<events.count {
        let event = HMLOrderEventsModel() 
       if let dic = events[i] as? NSDictionary{
        
        event.eventDateTime = dic["eventDateTime"] as? String
        event.eventStatus = dic["eventStatus"] as? String
        event.orderId = dic["orderId"] as? String
        event.statusMessage = dic["statusMessage"] as? String
        eventsArr.append(event)
        
        }
        
    }
    return eventsArr
    }
}
