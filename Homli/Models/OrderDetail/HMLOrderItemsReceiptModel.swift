//
//  HMLOrderItemsReceiptModel.swift
//  Homli
//
//  Created by daffolapmac on 22/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
class HMLOrderItemsReceiptModel {
    
    var plateCode             : String?
    var plateName             : String?
    var platePrice            : String?
    
    class func bindDataWithModel(events: NSArray) -> [HMLOrderItemsReceiptModel]
    {
        
        var arryObj = [HMLOrderItemsReceiptModel]()
        
        for i in 0..<events.count{
            
            let itemReceipt = HMLOrderItemsReceiptModel()
            
            if let dic = events[i] as? NSDictionary{
                
                itemReceipt.plateCode = dic["plateCode"] as? String
                itemReceipt.plateName = dic["plateName"] as? String
                itemReceipt.platePrice =  String(dic["platePrice"] as! Int)
                arryObj.append(itemReceipt)
                
            }
            
        }
        return arryObj
    }
}
