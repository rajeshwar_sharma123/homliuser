//
//  HMLOrderReceiptModel.swift
//  Homli
//
//  Created by daffolapmac on 22/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
class HMLOrderReceiptModel {
    
    var discountAmount         : String?
    var payAmount              : String?
    var receiptMessage         : String?
    var totalAmount            : String?
    var orderItemsReceipt      = []
    
    class func bindDataWithModel(receiptDic: NSDictionary) -> HMLOrderReceiptModel
    {
        let receipt = HMLOrderReceiptModel()
        receipt.discountAmount = String(receiptDic["discountAmount"] as! Int)
        receipt.payAmount = String(receiptDic["payAmount"] as! Int)
        receipt.receiptMessage = receiptDic["receiptMessage"] as? String
        receipt.totalAmount = String(receiptDic["totalAmount"] as! Int)
        receipt.orderItemsReceipt = HMLOrderItemsReceiptModel.bindDataWithModel((receiptDic["orderItemsReceipt"] as? NSArray)!)
        return receipt
    }
}
