//
//  HMLAddressComponentModel.swift
//  Homli
//
//  Created by daffolapmac on 15/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLAddressComponentModel: NSObject {
    
    var premise                       : String!
    var street_number                 : String!
    var route                         : String!
    var sublocality_level_3           : String!
    var sublocality_level_2           : String!
    var sublocality_level_1           : String!
    var locality                      : String!
    var administrative_area_level_2   : String!
    var administrative_area_level_1   : String!
    var country_long_name             : String!
    var country_short_name            : String!
    var postal_code                   : String!
    var latitude                      : String!
    var longitude                     : String!
    var isFromConfirmYourAddressScreen     = false
    
    var tmpAddressComponent           :  HMLAddressComponentModel?
    override init() {
        super.init()
      setInitialValue()
    }
    static let instance = HMLAddressComponentModel()
    
    class func sharedInstance() -> HMLAddressComponentModel
    {
        return instance;
    }

    func bindDataWithModel(dicAddress : NSDictionary)
    {
        print(dicAddress)
        
        
        let tmpArr : NSArray = dicAddress["address_components"] as! NSArray
        
        let locationDic = dicAddress["geometry"]!["location"] as! NSDictionary
        setInitialValue()
        
        self.latitude = String(locationDic["lat"] as! NSNumber)
        self.longitude = String(locationDic["lng"] as! NSNumber)
        
       
        
        for i in 0 ..< tmpArr.count {
            
            let dic : NSDictionary = tmpArr[i] as! NSDictionary
            let array = dic["types"]! as! NSArray
            switch(array[0] as! String) {
            case "premise":
                self.premise = dic["long_name"] as! String + ", "
            case "street_number":
                self.street_number = dic["long_name"] as! String + ", "
            case "route":
               self.route = dic["long_name"] as! String + ", "
            case "sublocality_level_3":
               self.sublocality_level_3 = dic["long_name"] as! String + ", "
            case "sublocality_level_2":
                self.sublocality_level_2 = dic["long_name"] as! String + ", "
            case "sublocality_level_1":
                self.sublocality_level_1 = dic["long_name"] as! String + ", "
            case "locality":
                self.locality = dic["long_name"] as! String 
            case "administrative_area_level_2":
                self.administrative_area_level_2 = dic["long_name"] as! String + ", "
            case "administrative_area_level_1":
                self.administrative_area_level_1 = dic["long_name"] as! String + ", "
            case "country":
                self.country_long_name = dic["long_name"] as! String
                self.country_short_name = dic["short_name"] as! String
            case "postal_code":
                self.postal_code = dic["long_name"] as! String
            default:
                print("raaj")
            }
         
        }
    }
    
    func setInitialValue()
    {
        self.premise                     = ""
        self.street_number               = ""
        self.route                       = ""
        self.sublocality_level_3         = ""
        self.sublocality_level_2         = ""
        self.sublocality_level_1         = ""
        self.locality                    = ""
        self.administrative_area_level_2 = ""
        self.administrative_area_level_1 = ""
        self.country_long_name           = ""
        self.country_short_name          = ""
        self.postal_code                 = ""
        self.latitude                    = ""
        self.longitude                   = ""
        isFromConfirmYourAddressScreen     = false

    }
    
    
    
}
