//
//  HMLPaceOrderModel.swift
//  Homli
//
//  Created by Daffolap-21 on 12/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation

class HMLPlaceOrderModel {

	static var instance = HMLPlaceOrderModel()
	// SHARED INSTANCE
	class func sharedInstance() -> HMLPlaceOrderModel {
		self.instance = (self.instance ?? HMLPlaceOrderModel())

		return self.instance
	}

	var userMobile: NSString?
	var promoCode: NSString?
	var deliveryAddressId: NSString?
	var totalAmount: Double! = 0.0
	var discountAmount: Double! = 0.0
	var payAmount: Double! = 0.0
	var deliverySlot: NSString?
	var selectedSlotIndex: Int? = 0
	var selectedPaymentModeIndex: Int?
	var deliverySlotStartTime: NSString?
	var deliverySlotEndTime: NSString?
	var paymentInfo: NSString?
	var paymentMethod: NSString?
	var orderItems: [HMLPlateDetailModel] = [];
	var deliveryAddress: NSDictionary?
	// var selectedDict = [Int:Int]()
	var plateUpdated: Bool = false

	func getPlateIfExist(plate: HMLPlateDetailModel) -> HMLPlateDetailModel
	{
		for  i in 0 ..< orderItems.count {

			let plateTmp = orderItems[i]
			if plate.plateCode == plateTmp.plateCode {
				return plateTmp
			}

			}
			return HMLPlateDetailModel()
		}

		func initWithDefaultValue() {
			promoCode = nil
			totalAmount = 0.0
			discountAmount = 0.0
			payAmount = 0.0
			deliverySlot = nil
			selectedSlotIndex = 0
			selectedPaymentModeIndex = nil
			deliverySlotStartTime = nil
			deliverySlotEndTime = nil
			paymentInfo = nil
			paymentMethod = nil
			orderItems = []
			plateUpdated = false
		}

	}

