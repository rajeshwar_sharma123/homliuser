//
//  String+Utility.swift
//  Homli
//
//  Created by daffolapmac on 16/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
extension String
{
	func truncateLastTwoCharacter() -> String
	{
		if (!self.isEmpty || self.characters.count > 2) {
			let endIndex = self.endIndex.advancedBy(-2)
			return self.substringToIndex(endIndex)
		}
		return ""
	}

	func convertToDate() -> String
	{
		let formatter: NSDateFormatter = NSDateFormatter()
		formatter.dateFormat = "d-MM-yyyy"
		let date: NSDate = formatter.dateFromString(self)!
		formatter.dateFormat = "d MMMM yyyy"
		let newDate: String = formatter.stringFromDate(date)
		return newDate
	}
//    func convertToStringFormat()-> String{
//        let formatter: NSDateFormatter = NSDateFormatter()
//        formatter.dateFormat = "d-MM-yyyy"
//        // let date: NSDate = formatter.da
//        formatter.dateFormat = "d MMMM yyyy"
//        let newDate: String = formatter.stringFromDate(self)
//        return newDate
//    }

	func setIndianCurrencyStyle() -> String {
		let price = Double(self)
		let formatter = NSNumberFormatter()
		formatter.numberStyle = .CurrencyStyle
		formatter.locale = NSLocale(localeIdentifier: "en_IN")
		return formatter.stringFromNumber(price!)!
	}

	func convertToDoubleValue() -> Double {
		// let locale = NSLocale.currentLocale()
		let formatter = NSNumberFormatter()
		formatter.locale = NSLocale(localeIdentifier: "en_IN")
		formatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
		let moneyDouble = formatter.numberFromString(self)?.doubleValue
		return moneyDouble!
	}

	func subStringWirthRange(startIndex: Int, endIndex: Int) -> String {
		let subString = self.substringWithRange(Range<String.Index>(start: self.startIndex.advancedBy(startIndex), end: self.endIndex.advancedBy(-endIndex)))
		return subString
	}

	func subStringFromIndex(index: Int) -> String {
		let subString = (self as NSString).substringFromIndex(index)
		return subString as String
	}
	func trim() -> String {
		return self.stringByTrimmingCharactersInSet(
			NSCharacterSet.whitespaceAndNewlineCharacterSet())
	}

	func getDateFormTimeString() -> String
	{
		// let timeString = "04:05 PM"
		// let fullName = "First Last"

		let timeSpittedArray = self.characters.split { $0 == ":" || $0 == " " }.map(String.init)

		debugPrint("splitted array: \(timeSpittedArray)")

		let date = NSDate().toLocalTime()
		let calendar = NSCalendar.currentCalendar()
		let components = calendar.components([.Minute, .Hour, .Day, .Month, .Year, .TimeZone], fromDate: date)

		if timeSpittedArray.count >= 2
		{
			components.minute = Int(timeSpittedArray[1])!
			components.hour = Int(timeSpittedArray[0])!
			components.timeZone = NSTimeZone.localTimeZone()
		}

		let newDate = calendar.dateFromComponents(components)!
		return newDate.convertToLocalTimeZone()
	}
}
