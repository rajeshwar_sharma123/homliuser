//
//  UIImageView+Utility.swift
//  Homli
//
//  Created by daffolapmac on 21/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

//import Foundation
extension UIImageView
{
 
    func setImageColorFromHexaCode(hexaValue: UInt)
    {
        self.image = self.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        self.tintColor = ColorFromHexaCode(hexaValue)
    }
    
}