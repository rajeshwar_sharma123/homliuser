//
//  UIViewController+Utility.swift
//  Homli
//
//  Created by daffolapmac on 14/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import JKNotificationPanel
extension UIViewController
{

  func showErrorMessage(message :String)
  {
    let panel = JKNotificationPanel()
    let view = panel.defaultView(.FAILED, message: message)
    view.setColor(UIColor.redColor())
    panel.showNotify(withView: view, belowNavigation: self.navigationController!)

    // let navView = self.navigationController?.view
    //panel.showNotify(withView: view, inView: navView!)


  }
  func showSuccessMessage(message :String)
  {
    JKNotificationPanel().showNotify(withStatus: .SUCCESS, belowNavigation: self.navigationController!, message: message)
  }

  func toggleLeftSide()
  {
    self.menuContainerViewController.toggleLeftSideMenuCompletion { () -> Void in

    }
  }

  func navigateToHomeViewController()
  {
    let yourOrderViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMLYourOrderViewController") as? HMLYourOrderViewController
    self.menuContainerViewController.centerViewController.pushViewController(yourOrderViewController!, animated: true)

  }
    func popToViewController(aClass:AnyClass){
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(aClass) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }

  func pushViewController(withIdentifireName identifire:NSString, inNavigationController nv:UINavigationController)
  {
    let viewController = self.storyboard!.instantiateViewControllerWithIdentifier(identifire as String)
    nv.pushViewController(viewController, animated: true)

  }
  func enableUserInteraction()
  {
    UIApplication.sharedApplication().endIgnoringInteractionEvents()
  }
  func disableUserInteraction()
  {
    UIApplication.sharedApplication().beginIgnoringInteractionEvents()
  }

    func setSideMenuPanModeNone(){
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
    }
    func setSideMenuPanModeDefault(){
        self.menuContainerViewController.panMode = MFSideMenuPanModeDefault
    }

    
}