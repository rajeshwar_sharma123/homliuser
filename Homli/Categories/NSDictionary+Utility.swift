//
//  NSDictionary+Utility.swift
//  Homli
//
//  Created by daffolapmac on 14/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
extension NSDictionary
{
 func removeAllNullValues() -> NSMutableDictionary
{
    let muDic: NSMutableDictionary = self.mutableCopy() as! NSMutableDictionary
    var keysToRemove = muDic.allKeys
    for i in 0 ..< keysToRemove.count {
        if(muDic.objectForKey(keysToRemove[i]) == nil || muDic.objectForKey(keysToRemove[i])!.classForCoder == NSNull.classForCoder())
        {
            muDic.removeObjectForKey(keysToRemove[i])
            
        }
    
    }
       return muDic
    }
}
