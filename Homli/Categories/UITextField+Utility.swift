//
//  UITextField+Utility.swift
//  Homli
//
//  Created by daffolapmac on 09/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

extension UITextField
{

    func leftPadding(width : CGFloat){
        let paddingView = UIView(frame:CGRectMake(0, 0, width, self.frame.size.height))
        self.leftView=paddingView;
        //paddingView.backgroundColor = UIColor.redColor()
        self.leftViewMode = UITextFieldViewMode.Always
    
    }

}
