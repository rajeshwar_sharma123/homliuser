//
//  HMLDate.swift
//  Homli
//
//  Created by Jenkins on 3/15/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation

extension NSDate
{
 func convertToStringFormat()-> String{
    let formatter: NSDateFormatter = NSDateFormatter()
    formatter.dateFormat = "d-MM-yyyy"
   // let date: NSDate = formatter.da
    formatter.dateFormat = "d MMMM yyyy"
    let newDate: String = formatter.stringFromDate(self)
    return newDate
    }

  func convertToLocalTimeZone() -> String
  {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy-HH:mm zzz"
    return dateFormatter.stringFromDate(self)
  }

  func toLocalTime() -> NSDate {
    let timeZone = NSTimeZone.localTimeZone()
    let seconds : NSTimeInterval = Double(timeZone.secondsFromGMTForDate(self))
    let localDate = NSDate(timeInterval: seconds, sinceDate: self)
    return localDate
  }
}

