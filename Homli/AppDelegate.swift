//
//  AppDelegate.swift
//  Homli
//
//  Created by daffolapmac on 08/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//
import UIKit
import IQKeyboardManagerSwift
import MFSideMenu

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	var navController: UINavigationController?
	var window: UIWindow?
	var sideMenuContainerViewController: MFSideMenuContainerViewController?

	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		// Override point for customization after application launch.

		UINavigationBar.appearance().barTintColor = ColorFromHexaCode(0x262626)
		UINavigationBar.appearance().tintColor = UIColor.whiteColor()
		UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]

		IQKeyboardManager.sharedManager().enable = true
		GMSServices.provideAPIKey(Google_API_Key)
		setRootViewController();

		return true
	}

	func applicationWillResignActive(application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}

	func applicationWillEnterForeground(application: UIApplication) {
		// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}

	func applicationWillTerminate(application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}
	func setRootViewController()
	{
		// self.window
		self.window = UIWindow(frame: UIScreen.mainScreen().bounds)

		// mainStoryboard
		let storyboard = UIStoryboard(name: "Main", bundle: nil)

		// rootViewController
		var viewController = UIViewController()
		HMLUtlities.setUserFromAddressScreen(false)
		if HMLUtlities.isUserLogedIn() {
			viewController = (storyboard.instantiateViewControllerWithIdentifier("HMLYourOrderViewController") as? HMLYourOrderViewController)!

		}
		else {
			viewController = (storyboard.instantiateViewControllerWithIdentifier("HMLJoinUsViewController") as? HMLJoinUsViewController)!

		}

		// navigationController
		let navigationController = UINavigationController(rootViewController: viewController)
		navController = navigationController

		let leftViewController = storyboard.instantiateViewControllerWithIdentifier("HMLLeftMenuViewController") as? HMLLeftMenuViewController

		sideMenuContainerViewController = MFSideMenuContainerViewController.containerWithCenterViewController(navigationController, leftMenuViewController: leftViewController, rightMenuViewController: nil);

		sideMenuContainerViewController?.panMode = MFSideMenuPanModeDefault
		sideMenuContainerViewController?.shadow.enabled = true;
		sideMenuContainerViewController?.setMenuWidth(self.window!.bounds.size.width * 0.82, animated: true);

		self.window!.rootViewController = sideMenuContainerViewController

		self.window!.makeKeyAndVisible()
	}

	//func removeLazyLoading()
	//{
//		var str: String = Token
//		str = UserProfile
//		str = ContactNumber
//		str = SignUpUser
//	}

}

