//
//  HMLAppServices.swift
//  Homli
//
//  Created by daffolapmac on 11/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HMLAppServices: NSObject {
    
    static let instance = HMLAppServices()
    
    class func sharedInstance() -> HMLAppServices
    {
        return instance;
    }

    func postServiceRequest(
        urlString urlString: String, parameter:AnyObject,
        successBlock:(responseData: AnyObject)->(),
        errorBlock:(errorMessage: NSError)->()
        )
    {

        let request = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameter, options: [])
        
        Alamofire.request(request)
            .responseJSON { response in
              
                switch response.result {
               
                case .Failure(let error):
                    
                      errorBlock(errorMessage: error)
                    
                case .Success(let responseObject):
                   
                    print(responseObject)
                    
                    successBlock(responseData: responseObject as! NSDictionary)
                    

                }
        }
 
    }
    
  class  func postServiceRequestWithNoResponse(
        urlString urlString: String, parameter:AnyObject,
        successBlock:(responseData: AnyObject)->(),
        errorBlock:(errorMessage: NSError)->()
        )
    {
        
        let request = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameter, options: [])
        
        Alamofire.request(request)
            .response { request, response, data, error in
                print(request)
                if (response != nil) {
                    successBlock(responseData:response!)
                }
                
                print(error)
        }
    }

    
 class func getServiceRequest(
        urlString urlString: String ,
        successBlock:(responseData: AnyObject)->(),
        errorBlock:(errorMessage: NSError)->()
        )
    {
        let request = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        request.HTTPMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
        Alamofire.request(request)
            .responseJSON { response in
                
                switch response.result {
                    
                case .Failure(let error):
                    
                    errorBlock(errorMessage: error)
                    
                case .Success(let responseObject):

//                  if let httpError = response.result.error {
//                    let statusCode = httpError.code
//                    if (statusCode == 200 || statusCode == 202)
//                    {
//                      successBlock(responseData: responseObject as! NSDictionary)
//                    }
//                    else
//                    {
//                      errorBlock(errorMessage: response.result.error!)
//                    }
//                  } else { //no errors
//                    let statusCode = (response.response?.statusCode)!
//                    if (statusCode == 200 || statusCode == 202)
//                    {
//                      successBlock(responseData: responseObject as! NSDictionary)
//                    }
//                    else
//                    {
//                      let errorObj:NSError = response.result.error!
//                      errorBlock(errorMessage: errorObj)
//                    }
//                  }

                    print(responseObject)
                    
                   successBlock(responseData: responseObject)
                }

      }
        
    }
    
    
}
