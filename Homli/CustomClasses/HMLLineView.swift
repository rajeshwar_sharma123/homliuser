//
//  HMLLineView.swift
//  Homli
//
//  Created by daffolapmac on 18/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//
//F0AB4F
import Foundation
import UIKit
class HMLLineView : UIView{
     required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = ColorFromHexaCode(0xF0AB4F)
    }
}