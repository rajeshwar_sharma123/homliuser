//
//  HMLTextField.swift
//  Homli
//
//  Created by daffolapmac on 09/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
protocol HMLTextFieldDelegate{
    
    func rightButtonAction()
    
}
class HMLTextField: UITextField,UITextFieldDelegate {
    var delegateObj: HMLTextFieldDelegate! = nil
    required init?(coder aDecoder: NSCoder){
    
        super.init(coder: aDecoder)
        self.backgroundColor = ColorFromHexaCode(0xF4F4F4)
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
        self.leftPadding(25)
        
    }
    
    func addButtonOnLeftWithImageName(name:String)
    {
        let paddingView = UIView(frame:CGRectMake(0, 0,46, self.frame.size.height))
        let button   = UIButton(type: UIButtonType.System) as UIButton
        button.enabled = false
        button.frame = CGRectMake(22, (self.frame.height-20)/2, 16, 20)
        self.leftViewMode = UITextFieldViewMode.Always
      
        button.setBackgroundImage(UIImage(named: name), forState: UIControlState.Normal)
        paddingView.addSubview(button)
        self.leftView = paddingView

    }
    
    func addButtonOnRightWithImageName(name: String)
    {
        let paddingView = UIView(frame:CGRectMake(0, 0,46, self.frame.size.height))
        let button   = UIButton(type: UIButtonType.System) as UIButton
        button.frame = CGRectMake(10, (self.frame.height-22)/2, 22, 22)
        self.rightViewMode = UITextFieldViewMode.Always
        button.setBackgroundImage(UIImage(named: name), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(HMLTextField.rightButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        paddingView.addSubview(button)
        self.rightView = paddingView
    }
    func rightButtonAction(sender:UIButton!)
    {
        delegateObj.rightButtonAction()
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
