//
//  HMLConfirmDeliveryAddressView.swift
//  Homli
//
//  Created by daffolapmac on 15/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import TextFieldEffects
class HMLConfirmDeliveryAddressView: UIView, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

	let pickerData = ["Lower Parel", "Lalbaug", "Prabhadevi"]
	var myPicker: UIPickerView!
	var shouldPopToAddressListingVC: Bool! = false
	var returnKeyHandler: IQKeyboardReturnKeyHandler!
	@IBOutlet var contentView: UIView!
	@IBOutlet var tableView: UITableView!
	var formatedAddressDic: NSDictionary?
	var deliveryAddrss = HMLDeliveryAddressModel.sharedInstance()
	let addressComponent = HMLAddressComponentModel.sharedInstance()
	// var  tmpDeliveryAddrss : HMLDeliveryAddressModel!
	// var tmpAddressComponent : HMLAddressComponentModel!

	var locality = ""
	/*
	 // Only override drawRect: if you perform custom drawing.
	 // An empty implementation adversely affects performance during animation.
	 override func drawRect(rect: CGRect) {
	 // Drawing code
	 }
	 */
	// func init
	func initDefaultValue(dicAddress: NSDictionary)
	{
		if !deliveryAddrss.flat_building_name.isEmpty {

			holdAddressTemperory()
		}

		deliveryAddrss.flat_building_name = ""
		deliveryAddrss.locality = ""

		returnKeyHandler = IQKeyboardReturnKeyHandler(controller: HMLUtlities.getTopViewController())
		myPicker = UIPickerView()
		contentView.layer.cornerRadius = 5
		contentView.clipsToBounds = true
		self.tableView.registerNib(UINib(nibName: "HMLConfirmDeliveryAddressCell", bundle: nil), forCellReuseIdentifier: "HMLConfirmDeliveryAddressCell")
		myPicker.delegate = self
		myPicker.dataSource = self

	}

	func holdAddressTemperory() {

		deliveryAddrss.tmpDeliveryAddrss = HMLDeliveryAddressModel()
		deliveryAddrss.tmpDeliveryAddrss.flat_building_name = deliveryAddrss.flat_building_name
		deliveryAddrss.tmpDeliveryAddrss.address_line_1 = deliveryAddrss.address_line_1
		deliveryAddrss.tmpDeliveryAddrss.address_line_2 = deliveryAddrss.address_line_2
		deliveryAddrss.tmpDeliveryAddrss.locality = deliveryAddrss.locality
		deliveryAddrss.tmpDeliveryAddrss.landmark = deliveryAddrss.landmark
		deliveryAddrss.tmpDeliveryAddrss.postal_code = deliveryAddrss.postal_code

	}

	// MARK:- TableView Delegate Method

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 6
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		let cell: HMLConfirmDeliveryAddressCell! = tableView.dequeueReusableCellWithIdentifier("HMLConfirmDeliveryAddressCell") as? HMLConfirmDeliveryAddressCell
		cell.selectionStyle = UITableViewCellSelectionStyle.None
		configureCell(cell, atIndexPath: indexPath)
		return cell

	}
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 60
	}

	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		print("You selected cell #\(indexPath.row)!")
	}
	// MARK:- Custom Method

	func configureCell(cell: HMLConfirmDeliveryAddressCell, atIndexPath indexPath: NSIndexPath)
	{

		let txtFld = cell.txtField
		txtFld.delegate = self
		txtFld.tag = indexPath.row
		var strAddress = String()
		switch (indexPath.row) {
		case 0:
			txtFld.placeholder = "Flat No / Building Name"
			// deliveryAddrss.flat_building_name = ""
			txtFld.keyboardType = UIKeyboardType.ASCIICapable

			if deliveryAddrss.flat_building_name == nil
			{
				txtFld.text = ""// deliveryAddrss.flat_building_name
			}
			else {
				// deliveryAddrss.flat_building_name = ""
				txtFld.text = deliveryAddrss.flat_building_name
			}
		case 1:
			txtFld.placeholder = "Address Line 1"
			strAddress = addressComponent.premise + addressComponent.street_number + addressComponent.route + addressComponent.sublocality_level_3
			txtFld.text = strAddress.truncateLastTwoCharacter()
			deliveryAddrss.address_line_1 = strAddress
		case 2:
			txtFld.placeholder = "Address Line 2"
			strAddress = addressComponent.sublocality_level_1 + addressComponent.sublocality_level_2
			txtFld.text = strAddress.truncateLastTwoCharacter()
			deliveryAddrss.address_line_2 = strAddress
		case 3:
			txtFld.placeholder = "Select Locality"
			txtFld.text = deliveryAddrss.locality
			txtFld.inputView = myPicker
			txtFld.addDoneOnKeyboardWithTarget(self, action: #selector(HMLConfirmDeliveryAddressView.doneAction(_:)), shouldShowPlaceholder: true)
		case 4:
			txtFld.placeholder = "Landmark"
			strAddress = addressComponent.sublocality_level_1
			txtFld.text = strAddress.truncateLastTwoCharacter()
			deliveryAddrss.landmark = strAddress
			txtFld.keyboardType = UIKeyboardType.ASCIICapable
		case 5:
			txtFld.placeholder = "Postal Code"
			txtFld.text = addressComponent.postal_code
			deliveryAddrss.postal_code = txtFld.text
			txtFld.keyboardType = UIKeyboardType.NumberPad
		default:
			txtFld.placeholder = ""
		}

	}

	// MARk: - UITextFieldDelegate

	func textFieldDidEndEditing(textField: UITextField) {

		let tag = textField.tag
		switch (tag) {
		case 0:
			deliveryAddrss.flat_building_name = textField.text
		case 1:
			deliveryAddrss.address_line_1 = textField.text
		case 2:
			deliveryAddrss.address_line_2 = textField.text
			// case 3:
			// deliveryAddrss.locality = textField.text
		case 4:
			deliveryAddrss.landmark = textField.text
		case 5:
			deliveryAddrss.postal_code = textField.text
		default:
			print(" ")
		}

	}

	func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
		let inputText = textField.text! + string

		if textField.tag == 0
		{

			deliveryAddrss.flat_building_name = inputText
		}

		return true
	}

	// MARK:-  Button Action

	@IBAction func okButtonClicked(sender: AnyObject) {

		if (!HMLValidation.validateDeliveryAddress())
		{
			return
		}
		// check whther to add address on server or not
		if (self.shouldPopToAddressListingVC == true)
		{

			print("address should be added on server")
			callServiceToAddAddress()

		}
		else
		{

			addressComponent.isFromConfirmYourAddressScreen = true
			NSNotificationCenter.defaultCenter().postNotificationName("DeliveryAddressNotification", object: nil)
			let vc = HMLUtlities.getTopViewController()
			vc.navigationController?.popViewControllerAnimated(true)
		}
	}

	@IBAction func cancelButtonClicked(sender: AnyObject) {

		if addressComponent.tmpAddressComponent != nil {

			addressComponent.street_number = addressComponent.tmpAddressComponent!.street_number
			addressComponent.route = addressComponent.tmpAddressComponent!.route
			addressComponent.sublocality_level_3 = addressComponent.tmpAddressComponent!.sublocality_level_3
			addressComponent.sublocality_level_2 = addressComponent.tmpAddressComponent!.sublocality_level_2
			addressComponent.sublocality_level_1 = addressComponent.tmpAddressComponent!.sublocality_level_1
			addressComponent.locality = addressComponent.tmpAddressComponent!.locality
			addressComponent.administrative_area_level_2 = addressComponent.tmpAddressComponent!.administrative_area_level_2
			addressComponent.administrative_area_level_1 = addressComponent.tmpAddressComponent!.administrative_area_level_1
			addressComponent.country_long_name = addressComponent.tmpAddressComponent!.country_long_name
			addressComponent.country_short_name = addressComponent.tmpAddressComponent!.country_short_name
			addressComponent.postal_code = addressComponent.tmpAddressComponent!.postal_code
			addressComponent.latitude = addressComponent.tmpAddressComponent!.latitude
			addressComponent.longitude = addressComponent.tmpAddressComponent!.longitude

		}
		if deliveryAddrss.tmpDeliveryAddrss != nil {
			deliveryAddrss.flat_building_name = deliveryAddrss.tmpDeliveryAddrss.flat_building_name
			deliveryAddrss.address_line_1 = deliveryAddrss.tmpDeliveryAddrss.address_line_1
			deliveryAddrss.address_line_2 = deliveryAddrss.tmpDeliveryAddrss.address_line_2
			deliveryAddrss.locality = deliveryAddrss.tmpDeliveryAddrss.locality
			deliveryAddrss.landmark = deliveryAddrss.tmpDeliveryAddrss.landmark
			deliveryAddrss.postal_code = deliveryAddrss.tmpDeliveryAddrss.postal_code
		}

		self.removeFromSuperview()
	}

	// MARK: - Delegates and data sources
	// MARK: Data Sources
	func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
		return 1
	}
	func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return pickerData.count
	}
	// MARK: Delegates
	func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		return pickerData[row]
	}

	func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		locality = pickerData[row]

	}

	func doneAction(sender: AnyObject) {
		deliveryAddrss.locality = locality
		self.tableView.reloadData()

	}
	// func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
	// let titleData = pickerData[row]
	// let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 26.0)!,NSForegroundColorAttributeName:UIColor.blueColor()])
	// return myTitle
	// }
	// MARK:- Service Calling For Add Address
	func callServiceToAddAddress()
	{
		let parameters = [
			"mobileNumber": HMLUtlities.getUserContactNumber(),
			"addrField1": "\(deliveryAddrss.address_line_1)",
			"addrField2": "\(deliveryAddrss.address_line_2)",
			"country": "IN",
			"pincode": "\(deliveryAddrss.postal_code)",
			"landmark": "\(deliveryAddrss.landmark)",
			"locality": "\(deliveryAddrss.locality)",
			"latitude": "\(HMLAddressComponentModel.sharedInstance().latitude)",
			"longitude": "\(HMLAddressComponentModel.sharedInstance().longitude)",
			"primary": "N"

		]

		print("prameters: \(parameters)")

		let url: String = HMLServiceUrls.getCompleteUrlFor(Url_To_Add_Address)

		self.showLoader()
		HMLAppServices.sharedInstance().postServiceRequest(urlString: url, parameter: parameters, successBlock: { (responseData) -> () in

			let response: NSDictionary = responseData as! NSDictionary
			self.hideLoader()
			print(response)
			let responseStatus = response["response"] as? String

			if responseStatus == "SUCCESS"
			{
				HMLUtlities.showSuccessMessage(response["message"] as! String)
				let vc = HMLUtlities.getTopViewController()
				vc.navigationController?.popViewControllerAnimated(true)
			}
			else {
				HMLUtlities.showErrorMessage(response["message"] as! String)
			}

		}) { (errorMessage) -> () in

			self.hideLoader()
			HMLUtlities.showErrorMessage(errorMessage.localizedDescription)

		}

	}
}
