//
//  HMLPastOrderTableViewCell.swift
//  Homli
//
//  Created by Daffolap-21 on 08/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLPastOrderTableViewCell: UITableViewCell {

  @IBOutlet weak var plateNameLabel: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var orderStatus: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()
    orderStatus.layer.cornerRadius = 3
    orderStatus.clipsToBounds = true
    orderStatus.layer.cornerRadius = 3
    orderStatus.clipsToBounds = true
    self.selectionStyle = UITableViewCellSelectionStyle.None
  }

  //MARK: UIViewCell configration
  /**
  - Parameter sender: Reference of indexPath & order which is class
  */

  func configureCell(indexPath : NSIndexPath ,order: Order)
  {
    if (order.orderItems.count <= 1)
    {
      self.plateNameLabel.text = order.orderItems[0]["plateName"] as? String
    }
    else {
     let diplayText = String.localizedStringWithFormat("\(order.orderItems[0]["plateName"]) and \(order.orderItems.count-1) more")

      self.plateNameLabel.text = diplayText
    }
    print(self.plateNameLabel.text)
    let currentDate = NSDate() as NSDate
    self.dateLabel.text = currentDate.convertToStringFormat()
   if order.status == "Delivered"
   {
    self.orderStatus.backgroundColor = ColorFromHexaCode(0x69A949)
    self.orderStatus.text = order.status
    }
   else{
    self.orderStatus.backgroundColor = ColorFromHexaCode(0xEF5350)
    self.orderStatus.text = order.status
    }
  }
  //MARK:
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }




}
