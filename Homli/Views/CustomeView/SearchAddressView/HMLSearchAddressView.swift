//
//  HMLSearchAddressView.swift
//  Homli
//
//  Created by daffolapmac on 05/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import GoogleMaps
protocol HMLSearchAddressViewDelegate {

	func getSearchedPlace(place: GMSPlace)
}
class HMLSearchAddressView: UIView, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, GMSAutocompleteFetcherDelegate {
	var fetcher: GMSAutocompleteFetcher?
	@IBOutlet var contentView: UIView!

	@IBOutlet var txtFldAddress: UITextField!
	@IBOutlet var tableView: UITableView!
	var addressArr = NSArray()
	var resultsArry = NSMutableArray()
	var placesClient = GMSPlacesClient()
	var delegate: HMLSearchAddressViewDelegate! = nil

	/*
	 // Only override drawRect: if you perform custom drawing.
	 // An empty implementation adversely affects performance during animation.
	 override func drawRect(rect: CGRect) {
	 // Drawing code
	 }
	 */
	override func layoutSubviews() {

		tableView.tableFooterView = UIView()
		contentView.layer.cornerRadius = 5
		contentView.clipsToBounds = true
		self.transparentBackground(withAlphaValue: 0.5)
		txtFldAddress.layer.borderWidth = 1
		txtFldAddress.layer.borderColor = ColorFromHexaCode(k_Theme_Color).CGColor
		txtFldAddress.layer.cornerRadius = txtFldAddress.frame.size.height / 2
		txtFldAddress.clipsToBounds = true
		txtFldAddress.leftPadding(25)

		self.tableView.registerNib(UINib(nibName: "HMLSearchAddressCell", bundle: nil), forCellReuseIdentifier: "HMLSearchAddressCell")

		// Set bounds to inner-west Sydney Australia.
		let neBoundsCorner = CLLocationCoordinate2D(latitude: -33.843366,
			longitude: 151.134002)
		let swBoundsCorner = CLLocationCoordinate2D(latitude: -33.875725,
			longitude: 151.200349)
		let bounds = GMSCoordinateBounds(coordinate: neBoundsCorner,
			coordinate: swBoundsCorner)

		// Set up the autocomplete filter.
		let filter = GMSAutocompleteFilter()
		filter.type = .NoFilter

		// Create the fetcher.
		fetcher = GMSAutocompleteFetcher(bounds: bounds, filter: filter)
		fetcher?.delegate = self

	}
	// MARK:- UITableViewDelegate Method
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return resultsArry.count
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		// let dic = dishesArray[indexPath.row] as! NSDictionary

		let prediction = resultsArry.objectAtIndex(indexPath.row) as! GMSAutocompletePrediction

		let cell: HMLSearchAddressCell! = tableView.dequeueReusableCellWithIdentifier("HMLSearchAddressCell") as? HMLSearchAddressCell

		cell.lblAddress.text = prediction.attributedFullText.string
		return cell

	}

	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 55

	}
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

		let prediction = resultsArry.objectAtIndex(indexPath.row) as! GMSAutocompletePrediction
		let placeID = prediction.placeID

		placesClient.lookUpPlaceID(placeID!, callback: { (place: GMSPlace?, error: NSError?) -> Void in
			if let error = error {
				print("lookup place id query error: \(error.localizedDescription)")
				return
			}

			if let place = place {

				self.delegate.getSearchedPlace(place)
				print("Place name \(place.coordinate.latitude)")
				print("Place name \(place.name)")
				print("Place address \(place.formattedAddress)")
				print("Place placeID \(place.placeID)")
				print("Place attributions \(place.attributions)")
			} else {
				print("No place details for \(placeID)")
			}
		})

		self.removeFromSuperview()
	}

	// MARK:- UITextFieldDelegate Method
	func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
		var addressName: NSString = textField.text! as NSString
		addressName = addressName.stringByReplacingCharactersInRange(range, withString: string)
		// getAddressListFromAddressName(addressName as String)
		fetcher?.sourceTextHasChanged(addressName as String)
		return true

	}
	// MARK:- Service Calling for getting the List From AddressName
	func getAddressListFromAddressName(addressName: String)
	{
		let urlStr: String = "https://maps.googleapis.com/maps/api/geocode/json?address=" + addressName
		HMLAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in

			print(responseData)
            //let arr: NSArray = responseData["results"] as! NSArray
//            if (arr.count > 0)
//            {
//                let formatedAddress :  String =  arr .firstObject!["formatted_address"] as! String
//                self.formatedAddressDic = arr .firstObject as! NSDictionary
//                self.lblAddress.text = formatedAddress
//                self.btnUseThisAddress.enabled = true
//            }

		}) { (errorMessage) -> () in

		}

	}

	@IBAction func cancelTapGestureAction(sender: AnyObject) {
		self.removeFromSuperview()
	}

	func didAutocompleteWithPredictions(predictions: [GMSAutocompletePrediction]) {
		resultsArry = NSMutableArray()
		let resultsStr = NSMutableString()
		for prediction in predictions {
			resultsStr.appendFormat("%@\n", prediction.attributedFullText.string)
			resultsArry.addObject(prediction)

		}
		print(resultsArry)
		self.tableView.reloadData()
		// txtFldAddress?.text = resultsStr as String
	}

	func didFailAutocompleteWithError(error: NSError) {
		// txtFldAddress?.text = error.localizedDescription
	}
}
