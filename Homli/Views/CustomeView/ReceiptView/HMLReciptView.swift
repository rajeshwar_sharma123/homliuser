//
//  HMLReciptView.swift
//  Homli
//
//  Created by Vishwas Singh on 3/18/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLReciptView: UIView,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var lblReceiptMsg: UILabel!
    @IBOutlet var dataTableView: UITableView!
    @IBOutlet var upperView: UIView!
    var itemReceipt = [HMLOrderItemsReceiptModel]()
    var receipt = HMLOrderReceiptModel()
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        dataTableView.registerNib(UINib(nibName: "HMLReceiptCell1", bundle: nil), forCellReuseIdentifier: "HMLReceiptCell1")
        dataTableView.registerNib(UINib(nibName: "HMLReceiptCell2", bundle: nil), forCellReuseIdentifier: "HMLReceiptCell2")
        dataTableView.rowHeight = UITableViewAutomaticDimension
        dataTableView.estimatedRowHeight = 65.0
        
    }
    func initDefaultValue(orderDetail:HMLOrderDetailModel)
    {
        receipt = orderDetail.receipt
        itemReceipt = receipt.orderItemsReceipt as! [HMLOrderItemsReceiptModel]
        lblReceiptMsg.text = receipt.receiptMessage
        self.dataTableView.delegate = self
        self.dataTableView.dataSource = self
        
    }
    
    //MARK:- TableView Data Source & Delegates Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemReceipt.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell : HMLReceiptCell1 = tableView.dequeueReusableCellWithIdentifier("HMLReceiptCell1") as! HMLReceiptCell1!
        

        let plateDetail = itemReceipt[indexPath.row] as HMLOrderItemsReceiptModel
        cell.configureCell(indexPath, item: plateDetail)
        return cell
        
    }
    
    
     func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
      let cell =  NSBundle.mainBundle().loadNibNamed("HMLReceiptCell2", owner: nil, options: nil)[0] as! HMLReceiptCell2
        cell.frame = CGRectMake(0, 0, tableView.frame.size.width, 84)
        cell.configureCell(NSIndexPath(forRow: 0, inSection: 0), receipt: receipt)
       // cell.backgroundColor = UIColor.redColor()
        return cell
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 90
    }
}
