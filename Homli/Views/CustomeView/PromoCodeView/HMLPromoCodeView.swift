//
//  HMLPromoCodeView.swift
//  Homli
//
//  Created by Daffolap-21 on 13/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLPromoCodeView: UIView {

  @IBOutlet weak var promoTextField: UITextField!

  @IBOutlet weak var applyButton: UIButton!

  @IBOutlet weak var promoInnerView: UIView!

  @IBOutlet weak var cancelButton: UIButton!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
  @IBOutlet weak var promoImageView: UIImageView!

  @IBOutlet weak var promoAlertView: UIView!

    override func layoutSubviews() {
        super.layoutSubviews()
        self.promoInnerView.layer.borderWidth = 1
        self.promoInnerView.layer.borderColor = ColorFromHexaCode(k_Theme_Color).CGColor

    self.transparentBackground(withAlphaValue: 0.6)
    }

  @IBAction func didTapCancelButton(sender: AnyObject) {

    self.removeFromSuperview()
  }

}
