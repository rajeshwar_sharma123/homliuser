//
//  HMLYourCartUpdateView.swift
//  Homli
//
//  Created by Jenkins on 5/2/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
protocol HMLYourCartUpdateViewDelegate {
	func updateNumberOfPlate()
}

class HMLYourCartUpdateView: UIView {
	var placeOrderModelObj = HMLPlaceOrderModel.sharedInstance()
	var plateDetailObj: HMLPlateDetailModel!
	var delegate: HMLYourCartUpdateViewDelegate! = nil
	var selectedIndex: Int!
	@IBOutlet weak var contentView: UIView!
	@IBOutlet weak var updateView: UIView!
	@IBOutlet weak var btnDelete: UIButton!
	@IBOutlet weak var btnClose: UIButton!
	@IBOutlet weak var lblPlateName: UILabel!
	@IBOutlet weak var lblNumberOfPlate: UILabel!
	@IBOutlet weak var lblTotalPrice: UILabel!
	var initialNumOfPlate: Int!
	var initailTotalAmount: Double!

	override func layoutSubviews() {
		super.layoutSubviews()
		self.updateView.layer.borderWidth = 2
		self.updateView.layer.borderColor = ColorFromHexaCode(0xE9E9E8).CGColor
		self.updateView.layer.cornerRadius = 5

		self.contentView.clipsToBounds = true
		self.contentView.layer.cornerRadius = 2
	}
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.transparentBackground(withAlphaValue: 0.5)
	}

	class func initYourCartUpdateView(parentView: UIView, plateDetail: HMLPlateDetailModel, atIndex index: Int) -> HMLYourCartUpdateView {

		let yourCartUpdateView = NSBundle.mainBundle().loadNibNamed("HMLYourCartUpdateView", owner: self, options: nil)[0] as! HMLYourCartUpdateView
		yourCartUpdateView.frame = parentView.bounds
		parentView.addSubview(yourCartUpdateView)
		yourCartUpdateView.selectedIndex = index
		yourCartUpdateView.plateDetailObj = plateDetail
		yourCartUpdateView.lblPlateName.text = plateDetail.plateName
		yourCartUpdateView.lblNumberOfPlate.text = String(plateDetail.noOfPlates)
		yourCartUpdateView.lblTotalPrice.text = String(Double(plateDetail.noOfPlates) * Double(plateDetail.price!)!).setIndianCurrencyStyle()

		yourCartUpdateView.initialNumOfPlate = yourCartUpdateView.plateDetailObj.noOfPlates
		yourCartUpdateView.initailTotalAmount = HMLPlaceOrderModel.sharedInstance().totalAmount

		return yourCartUpdateView
	}

	@IBAction func btnCloseAction(sender: AnyObject) {
		plateDetailObj.noOfPlates = initialNumOfPlate
		HMLPlaceOrderModel.sharedInstance().totalAmount = initailTotalAmount
		self.removeFromSuperview()
	}

	@IBAction func btnDeleteAction(sender: AnyObject) {
		var totalPrice = HMLPlaceOrderModel.sharedInstance().totalAmount
		totalPrice = totalPrice - Double(plateDetailObj.noOfPlates) * Double(plateDetailObj.price!)!
		HMLPlaceOrderModel.sharedInstance().totalAmount = totalPrice
		// HMLPlaceOrderModel.sharedInstance().selectedDict.removeValueForKey(plateDetailObj.index)
		HMLPlaceOrderModel.sharedInstance().orderItems.removeAtIndex(selectedIndex)
		HMLPlaceOrderModel.sharedInstance().plateUpdated = true
		self.removeFromSuperview()
		delegate.updateNumberOfPlate()
	}

	@IBAction func btnPlusAction(sender: AnyObject) {

		var plateNum = Int(self.lblNumberOfPlate.text!)
		if (plateNum != 10 && plateNum < Int(plateDetailObj.availablePlates!)) {
			plateNum = plateNum! + 1
			self.lblNumberOfPlate.text = String(plateNum!)
			plateDetailObj.noOfPlates = plateNum!
			var totalPrice = HMLPlaceOrderModel.sharedInstance().totalAmount
			totalPrice = totalPrice + Double(plateDetailObj.price!)!
			HMLPlaceOrderModel.sharedInstance().totalAmount = totalPrice
			self.lblTotalPrice.text = String(Double(plateNum!) * Double(plateDetailObj.price!)!).setIndianCurrencyStyle()

			// HMLPlaceOrderModel.sharedInstance().selectedDict.updateValue(plateNum!, forKey: plateDetailObj.index)

		}
	}
	@IBAction func btnMinusAction(sender: AnyObject) {
		var plateNum = Int(self.lblNumberOfPlate.text!)
		if plateNum != 1 {
			plateNum = plateNum! - 1
			self.lblNumberOfPlate.text = String(plateNum!)

			plateDetailObj.noOfPlates = plateNum!
			var totalPrice = HMLPlaceOrderModel.sharedInstance().totalAmount
			totalPrice = totalPrice - Double(plateDetailObj.price!)!

			HMLPlaceOrderModel.sharedInstance().totalAmount = totalPrice
			self.lblTotalPrice.text = String(Double(plateNum!) * Double(plateDetailObj.price!)!).setIndianCurrencyStyle()

			// HMLPlaceOrderModel.sharedInstance().selectedDict.updateValue(plateNum!, forKey: plateDetailObj.index)
		}
	}
	/*  func setTotalPrice(index:Int){

	 let plate = self.platesDetail[index] as HMLPlateDetailModel
	 let price1 = lblTotalPrice.text!.convertToDoubleValue()
	 let price2 = Double(plate.price!)
	 lblTotalPrice.text =  String(price1 + price2!).setIndianCurrencyStyle()
	 }
	 func deductTotalPrice(index:Int){

	 let plate = self.platesDetail[index] as HMLPlateDetailModel
	 let price1 = lblTotalPrice.text!.convertToDoubleValue()
	 let price2 = Double(plate.price!)
	 lblTotalPrice.text =  String(price1 - price2!).setIndianCurrencyStyle()
	 }*/

	@IBAction func btnUpdateAction(sender: AnyObject) {
		self.removeFromSuperview()
		HMLPlaceOrderModel.sharedInstance().plateUpdated = true
		delegate.updateNumberOfPlate()
	}

}
