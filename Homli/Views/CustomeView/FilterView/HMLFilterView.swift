//
//  HMLFilterView.swift
//  Homli
//
//  Created by daffolapmac on 29/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
protocol HMLFilterViewDelegate{
    func getPreferencesToFilterItem(veg:Bool,nonVeg:Bool)
}
class HMLFilterView: UIView,UIGestureRecognizerDelegate {
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var contentView: UIView!
    var hiddenFlag: Bool = true
    var delegate:HMLFilterViewDelegate! = nil
    @IBOutlet var btnVeg: UIButton!
    @IBOutlet var btnNonVeg: UIButton!
    @IBOutlet var contentViewYconstraint: NSLayoutConstraint!
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    class func addFilterView(parrentView:UIView)->HMLFilterView{
        let filterView =  NSBundle.mainBundle().loadNibNamed("HMLFilterView", owner: self, options: nil)[0] as! HMLFilterView
        filterView.frame = parrentView.bounds
        parrentView.addSubview(filterView)
        filterView.backgroundColor = UIColor.clearColor()
        filterView.contentView.layer.cornerRadius = 2
        filterView.contentView.clipsToBounds = true
        filterView.contentView.layer.borderWidth = 2
        filterView.contentView.layer.borderColor = UIColor.lightGrayColor().CGColor
        filterView.btnDone.layer.cornerRadius = 2
        filterView.btnDone.clipsToBounds = true
        return filterView
    }
    
    func initWithDefaultValue()
    {
        
    }
    @IBAction func tapGestureClickedAction(sender: AnyObject) {
        hideView()
        
    }
    
    @IBAction func doneButtonClickedAction(sender: AnyObject) {
        hideView()
        let veg: Bool = btnVeg.tag==1 ? true: false
        let nonVeg: Bool = btnNonVeg.tag==1 ? true: false
        self.delegate.getPreferencesToFilterItem(veg,nonVeg: nonVeg)
    }
    func showView(){
        contentViewYconstraint.constant = -150
        self.contentView.layoutIfNeeded()
        UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.contentViewYconstraint.constant = -2
            self.contentView.layoutIfNeeded()
            self.hidden = false
            self.hiddenFlag = false
        }) { _ in
            
            
        }
    }
    func hideView(){
        
        UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.contentViewYconstraint.constant = -150
            self.contentView.layoutIfNeeded()
            
        }) { _ in
            self.hidden = true
            self.hiddenFlag = true
        }
        
    }
    
    func toggelView(){
        if hiddenFlag{
            showView()
            
        }else
        {
            hideView()
        }
    }
    
    // MARK:- Custome Button Action
    
    
    //=========vegbutton=======
    @IBAction func vegButtonAction(sender: AnyObject) {
        let btn : UIButton = sender as! UIButton
        if(btn.tag==0)
        {
            btn.tag = 1;
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_selected"), forState: UIControlState.Normal)
        }
        else{
            btn.tag = 0
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_unselected"), forState: UIControlState.Normal)
            
        }
    }
    
    //=======nonveg button=======
    @IBAction func nonVegButtonAction(sender: AnyObject) {
        let btn : UIButton = sender as! UIButton
        if(btn.tag==0)
        {
            btn.tag = 1;
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_selected"), forState: UIControlState.Normal)
        }
        else{
            btn.tag = 0
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_unselected"), forState: UIControlState.Normal)
            
        }
        
    }
    //MARK:- UIGestureRecognizerDelegate Method
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if touch.view!.isDescendantOfView(self.contentView){
            return false
        }
        return true
    }
    
}
