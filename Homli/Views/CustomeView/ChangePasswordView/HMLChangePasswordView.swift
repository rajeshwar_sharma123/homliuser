//
//  HMLChangePasswordView.swift
//  Homli
//
//  Created by Vishwas Singh on 3/18/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLChangePasswordView: UIView
{
    
    @IBOutlet var txtOldPassword: UITextField!
    @IBOutlet var txtNewPassword: UITextField!
    @IBOutlet var oldViewLine: UIView!
    @IBOutlet var newViewLine: UIView!
    
    required init(coder aDecoder: (NSCoder!))
    {
        super.init(coder: aDecoder)!
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.transparentBackground(withAlphaValue: 0.6)
    }
    @IBAction func changeButton(sender: AnyObject) {
        callServiceToChangePassword()
        
    }
    
    @IBAction func cancelButton(sender: AnyObject) {
        self.removeFromSuperview()
        
    }
    func textFieldDidChange(txtOldPassword: UITextField){
        
    }
    
    //MARK:- Service Calling for Change Password
    func callServiceToChangePassword()
    {
        if !HMLValidation.validateChangePassword(txtOldPassword.text!, newPassword: txtNewPassword.text!){
            
            return
        }
        self.showLoader()
        let parameters = [
            "mobileNumber": HMLUtlities.getUserContactNumber(),
            "newPassword": txtNewPassword.text!,
            "oldPassword":txtOldPassword.text!
        ]
        
        HMLAppServices.sharedInstance().postServiceRequest(urlString:HMLServiceUrls.getCompleteUrlFor(Url_Change_Password), parameter: parameters, successBlock: { (responseData) -> () in
            
            let response : NSDictionary = responseData as! NSDictionary
            print(response)
            self.hideLoader()
            
            let boolValue = response["success"] as! Bool
            self.removeFromSuperview()
            if boolValue{
                HMLUtlities.showSuccessMessage("Your password changed successfully")
            }
            else{
                
            }
            
        }) { (errorMessage) -> () in
            
            self.hideLoader()
            HMLUtlities.showErrorMessage(errorMessage.localizedDescription)
            
        }
    }
    
    
}
