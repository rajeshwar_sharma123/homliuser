//
//  HMLSignInHelpView.swift
//  Homli
//
//  Created by Vishwas Singh on 3/14/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLSignInHelpView: UIView {
    
    
    required init?(coder aDecoder: NSCoder){
        
        super.init(coder: aDecoder)
        self.transparentBackground(withAlphaValue: 0.3)
        
    }
    
      
    @IBAction func gotItButtonAction(sender: AnyObject) {
        
        print("got it")
        
        
    }
    @IBAction func tapGestureAction(sender: AnyObject) {
        self.removeFromSuperview()
    }
    
}
