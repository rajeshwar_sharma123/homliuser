//
//  HMLLetsEatView.swift
//  Homli
//
//  Created by Jenkins_New on 5/6/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
protocol HMLLetsEatViewDelegate{
    
    func letsEatAction()
}
class HMLLetsEatView: UIView {
   
    var delegate : HMLLetsEatViewDelegate!
    @IBOutlet weak var letsEatView: UIView!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    override func layoutSubviews() {
        super.layoutSubviews()
        letsEatView.layer.cornerRadius = 3
        letsEatView.clipsToBounds = true
    }
    class func initLetsEatView(parentView:UIView)->HMLLetsEatView{
        let letsEatView =  NSBundle.mainBundle().loadNibNamed("HMLLetsEatView", owner: self, options: nil)[0] as! HMLLetsEatView
        letsEatView.frame = parentView.bounds
        parentView.addSubview(letsEatView)
        return letsEatView
    }

    @IBAction func letsEatViewTapGestureAction(sender: AnyObject) {
        delegate.letsEatAction()
    }

}
