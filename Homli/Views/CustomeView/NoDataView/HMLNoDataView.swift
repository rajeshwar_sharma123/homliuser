//
//  HMLNoDataView.swift
//  Homli
//
//  Created by daffolapmac on 02/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLNoDataView: UIView {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    func initWithDefaultValue(title:String,description:String){
        lblTitle.text = title
        lblDescription.text = description
        
    }
    
    class func initNoDataView(parentView:UIView)->HMLNoDataView{
        let noDataView =  NSBundle.mainBundle().loadNibNamed("HMLNoDataView", owner: self, options: nil)[0] as! HMLNoDataView
        noDataView.frame = parentView.bounds
        return noDataView
    }


}
