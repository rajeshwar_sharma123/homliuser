//
//  HMLStatusView.swift
//  Homli
//12:22am
//  Created by Vishwas Singh on 3/18/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLStatusView: UIView,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var lblstatus: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblPinCode: UILabel!
    @IBOutlet var lblDate: UILabel!
    var events = [HMLOrderEventsModel]()
    
    @IBOutlet var historyView: UIView!
    
    @IBOutlet var statusView: UIView!
    var flag=0
    
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        flag=0
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.tableView.registerNib(UINib(nibName: "HMLStatusCell", bundle: nil), forCellReuseIdentifier: "HMLStatusCell")
        let verticalLineView = UIView()
        verticalLineView.frame = CGRectMake(10, 0, 1, 0)
        verticalLineView.backgroundColor = UIColor.redColor()
        
    }
    
    func initDefaultValue(orderDetail:HMLOrderDetailModel)
    {
        events = orderDetail.events as! [HMLOrderEventsModel]
        lblstatus.text = orderDetail.status
        lblDate.text = orderDetail.statusDate?.convertToDate()
        lblPinCode.text = "Pincode: " + (orderDetail.address!["pinCode"] as? String)!
        lblAddress.text = (orderDetail.address!["addressLine1"] as! String) + "," + (orderDetail.address!["addressLine2"] as! String)
        tableView.reloadData()
    }
    
    //MARK:- TableView Data Source & Delegates Method
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: HMLStatusCell! = tableView.dequeueReusableCellWithIdentifier("HMLStatusCell") as? HMLStatusCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        configureCell(cell, atIndexPath: indexPath)
        return cell
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  60
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
    }
    
    
    func configureCell(cell:HMLStatusCell, atIndexPath indexPath:NSIndexPath)
    {
        
        let event = events[indexPath.row] as HMLOrderEventsModel
        print(event)
        cell.lblTitle.text = event.eventStatus
        cell.lblDescription.text = event.statusMessage
        
             
    }
    // MARK:- Custom Method
    @IBAction func historyButton(sender: AnyObject) {
        if(flag==0)
        {
            historyView.hidden = true
            flag=1
        }
        else
        {
            historyView.hidden = false
            flag=0
        }
        
    }
}

