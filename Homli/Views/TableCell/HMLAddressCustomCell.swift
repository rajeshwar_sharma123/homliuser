//
//  HMLAddressCustomCell.swift
//  Homli
//
//  Created by Vishwas Singh on 3/21/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

protocol removeRowDelegate {
    func removeRow(didRemove: Int)
}
protocol showAlertDelegate {
    func showAlert()
}
class HMLAddressCustomCell: UITableViewCell {
    
    var delegate: removeRowDelegate?
    
    
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var removeRow: UIButton!
    @IBOutlet var DeleteButtonConstarint: NSLayoutConstraint!
    @IBOutlet var lblDelivery: UILabel!
    
    var currentCell:(Int) = 0
    override func awakeFromNib()
    {
        super.awakeFromNib()
        DeleteButtonConstarint.constant=0
        lblDelivery .hidden = true
        
    }
    
    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func removeRow(sender: AnyObject)
    {
        currentCell = sender.tag
        delegate?.removeRow(currentCell)
    }
    
    @IBAction func btnImage(sender: AnyObject) {
        let alert = UIAlertView()
        alert.delegate = self
        alert.title = "Are you sure?"
        alert.message = "You want to set \"\(lblAddress.text)\" as your primary Address."
        alert.addButtonWithTitle("Yes")
        alert.alertViewStyle = UIAlertViewStyle.Default
        alert.addButtonWithTitle("No")
        alert.show()
        
    }
    
}
