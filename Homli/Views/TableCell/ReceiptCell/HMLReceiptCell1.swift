//
//  HMLReceiptCell1.swift
//  Homli
//
//  Created by daffolapmac on 20/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLReceiptCell1: UITableViewCell {

    @IBOutlet var lblSerialNo: UILabel!
    @IBOutlet var lblPlateName: UILabel!
    @IBOutlet var lblPlatePrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyle.None

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(indexPath : NSIndexPath ,item: HMLOrderItemsReceiptModel)
    {
        self.lblPlateName.text = item.plateName
        self.lblPlatePrice.text = "Rs. " + item.platePrice! + ".00"
        self.lblSerialNo.text = String(indexPath.row)
        
        
    }
    
}
