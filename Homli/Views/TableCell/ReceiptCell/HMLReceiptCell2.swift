//
//  HMLReceiptCell2.swift
//  Homli
//
//  Created by daffolapmac on 21/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLReceiptCell2: UITableViewCell {

    @IBOutlet var lblSubTotal: UILabel!
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet var lblTotal: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyle.None

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(indexPath : NSIndexPath ,receipt: HMLOrderReceiptModel)
    {
        self.lblSubTotal.text = "Rs. " + receipt.payAmount! + ".00"
        self.lblDiscount.text = "Rs. " + receipt.discountAmount! + ".00"
        self.lblTotal.text =  "Rs. " + receipt.totalAmount! + ".00"
        
        
    }


}
