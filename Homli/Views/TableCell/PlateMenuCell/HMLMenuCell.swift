//
//  HMLMenuCell.swift
//  Homli
//
//  Created by daffolapmac on 18/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLMenuCell: UITableViewCell {

    @IBOutlet var lblPlateName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblPlateDescription: UILabel!
   
    @IBOutlet var lblCount: UILabel!
    @IBOutlet var btnRemove: UIButton!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var quantityView: UIView!
    @IBOutlet var cartView: UIView!
    @IBOutlet var btnCart: UIButton!
    @IBOutlet var ratingStarView: CosmosView!
    @IBOutlet var lblPlateAvailable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyle.None
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
