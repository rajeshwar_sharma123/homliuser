//
//  HMLStatusCell.swift
//  Homli
//
//  Created by daffolapmac on 19/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLStatusCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var imgViewTick: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgViewTick.setImageColorFromHexaCode(0x89C502)
        imgViewTick.layer.cornerRadius = imgViewTick.frame.width/2
        imgViewTick.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
