//
//  HMLYourOrderTableViewCell.swift
//  Homli
//
//  Created by Jenkins on 3/14/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLYourOrderTableViewCell: UITableViewCell {
   
    @IBOutlet weak var plateNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var orderPlaced: UILabel!
    @IBOutlet weak var cancelOrder: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        orderPlaced.layer.cornerRadius = 3
        orderPlaced.clipsToBounds = true
        cancelOrder.layer.cornerRadius = 3
        cancelOrder.clipsToBounds = true
        self.selectionStyle = UITableViewCellSelectionStyle.None
    }
    //MARK: UIViewCell configration
    /**
    - Parameter sender: Reference of indexPath & order which is class
    */


    func configureCell(indexPath : NSIndexPath ,order: Order)
    {
      //self.plateNameLabel.text = order.orderItems[0]["plateName"] as? String
      if (order.orderItems.count <= 1)
      {
        self.plateNameLabel.text = order.orderItems[0]["plateName"] as? String
      }
      else {
        let diplayText = "\(order.orderItems[0]["plateName"] as! String) and \(order.orderItems.count-1) more"
        self.plateNameLabel.text = diplayText
      }
        print(self.plateNameLabel.text)
        //let currentDate = NSDate() as NSDate
        //let currentDate =
        self.dateLabel.text = order.orderDate?.convertToDate()
      self.cancelOrder.tag = indexPath.row
      let isCancellable = order.isOrderCancellable(order.status!)
       if(isCancellable == true)
       {
        self.cancelOrder.hidden = false
      }
      else
       {
        self.cancelOrder.hidden = true
      }
      if(order.status == OrderStatus.DELIVERED.rawValue)
      {
        self.orderPlaced.backgroundColor = ColorFromHexaCode(0x69A949)
        self.orderPlaced.text = order.status
      }
      else{
        self.orderPlaced.backgroundColor = ColorFromHexaCode(0xEF5350)
        self.orderPlaced.text = order.status
      }
      self.orderPlaced.userInteractionEnabled = false

    }
    //MARK: 
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    


}
