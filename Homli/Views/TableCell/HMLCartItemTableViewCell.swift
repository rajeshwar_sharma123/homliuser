//
//  HMLCartItemTableViewCell.swift
//  Homli
//
//  Created by Daffolap-21 on 12/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLCartItemTableViewCell: UITableViewCell {

  @IBOutlet weak var indicatorImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var plateCountLabel: UILabel!
  @IBOutlet weak var descLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }

  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    // Configure the view for the selected state
  }

  func configureCell(indexPath: NSIndexPath, item:HMLPlateDetailModel) -> Void
  {
    self.nameLabel.text = item.plateName
    self.descLabel.text = item.plateDescription
    self.plateCountLabel.text = "\(item.noOfPlates)"
    let doubleValue : Double = NSString(string: item.price!).doubleValue
    let prise = String(doubleValue * Double(item.noOfPlates))
    self.priceLabel.text = prise.setIndianCurrencyStyle()

  }

}
