//
//  HMLSearchAddressCell.swift
//  Homli
//
//  Created by daffolapmac on 05/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLSearchAddressCell: UITableViewCell {

    @IBOutlet var lblAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
