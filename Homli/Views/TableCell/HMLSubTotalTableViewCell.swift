//
//  HMLTotalTableViewCell.swift
//  Homli
//
//  Created by Daffolap-21 on 13/04/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLSubTotalTableViewCell: UITableViewCell {


  @IBOutlet weak var subTotalLabel: UILabel!
  @IBOutlet weak var discountLabel: UILabel!
  @IBOutlet weak var totalLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

  func configureCellWithSubtotal(placeOrderObj: HMLPlaceOrderModel)
  {
    if(placeOrderObj.promoCode != nil)
    {
    self.subTotalLabel.text =  String(placeOrderObj.totalAmount).setIndianCurrencyStyle()
    self.discountLabel.text = String(placeOrderObj.discountAmount).setIndianCurrencyStyle()
    self.totalLabel.text = String((placeOrderObj.totalAmount)! - (placeOrderObj.discountAmount)!).setIndianCurrencyStyle()
    }
    else
    {
      self.subTotalLabel.text = ""
      self.discountLabel.text = ""
      //self.totalLabel.text = "Rs. \(placeOrderObj.totalAmount)"
      self.totalLabel.text = String(placeOrderObj.totalAmount).setIndianCurrencyStyle()
    }
  }
}
