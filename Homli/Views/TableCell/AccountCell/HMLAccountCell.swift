//
//  HMLAccountCell.swift
//  Homli
//
//  Created by daffolapmac on 22/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLAccountCell: UITableViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgView.layer.cornerRadius = imgView.frame.size.width/2
        imgView.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(indexPath : NSIndexPath ,arrayData : NSArray)
    {
        let dic = arrayData[indexPath.row]
        self.lblTitle.text = dic["title"] as? String
        self.lblDescription.text =  dic["description"] as? String
        self.imgView.image = UIImage(named: (dic["image"] as? String)!)

    
    }
}
