//
//  HMLValidation.swift
//  Homli
//
//  Created by daffolapmac on 14/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class HMLValidation: NSObject {
    
    /**
     This method is for SignInValidation
    */
    class func validateSignIn(contact: NSString, password: NSString)->Bool
    {
        if(contact.length == 0)
        {
            showErrorMessage(ENTER_CONTACT_NUMBER)
            return false
        }
        if(contact.length != 0)
        {
            if(!validateContactNumber(contact))
            {
                showErrorMessage(ENTER_VALID_CONTACT_NUMBER)
                return false
            }
        }
        if(password.length == 0)
        {
            showErrorMessage(ENTER_PASSWORD)
            return false
        }
        return true
    }
    
    
    /**
     This Method Validate the SignUp
     */
    
    class func validateSignUp(name: NSString, contact: NSString, email: NSString, password: NSString)->Bool
    {
        if(name.length == 0)
        {
            showErrorMessage(ENTER_NAME)
            return false
        }
        
        
        if(contact.length == 0)
        {
            showErrorMessage(ENTER_CONTACT_NUMBER)
            return false
        }
        
        
        if(contact.length != 0)
        {
            if(!validateContactNumber(contact))
            {
                showErrorMessage(ENTER_VALID_CONTACT_NUMBER)
                return false
            }
        }
        
        if(email.length == 0)
        {
            showErrorMessage(ENTER_EMAIL_ID)
            return false
        }
        
        if(email.length != 0)
        {
            if(!isValidEmail(email as String ))
            {
                showErrorMessage(ENTER_VALID_EMAIL_ID)
                return false
            }
        }
        
        if(password.length == 0)
        {
            showErrorMessage(ENTER_PASSWORD)
            return false
        }
        return true
    }
    
    /**
     This method will check OTP validation
     */
    class func validateOTP(otp: NSString) -> Bool
    {
        if(otp.length == 0)
        {
            showErrorMessage(ENTER_OTP)
            return false
        }
        return true
    }
    
    
    /**
     This Method Validate the DeliveryAddress
     */
    class func validateDeliveryAddress() -> Bool
    {
        let  deliveryAddrss = HMLDeliveryAddressModel.sharedInstance()
        if(deliveryAddrss.flat_building_name.trim().isEmpty)
        {
            showErrorMessage(ENTER_FLAT_BUILDING_NAME)
            return false
        }
        if(deliveryAddrss.address_line_1.trim().isEmpty)
        {
            showErrorMessage(ENTER_ADDRESS)
            return false
        }
        if(deliveryAddrss.locality.isEmpty)
        {
            showErrorMessage(ENTER_LOCALITY)
            return false
        }
        if(deliveryAddrss.landmark.trim().isEmpty)
        {
            showErrorMessage(ENTER_LANDMARK)
            return false
        }
        if(deliveryAddrss.postal_code.trim().isEmpty)
        {
            showErrorMessage(ENTER_POSTAL_CODE)
            return false
        }
        if(deliveryAddrss.postal_code.characters.count<6)
        {
            showErrorMessage(ENTER_VALID_POSTAL_CODE)
            return false
        }
      
        return true
    }
    
    /**
     This method validated the UserDetail
    */
    class func validateUserDetail(addressLine1:String, landMark:String, pincode:String, veg:Bool, nonVeg:Bool, lat:String, long:String, isFromConfirmYourAddressScreen:Bool) -> Bool
    {
        if(addressLine1.trim().isEmpty)
        {
            showErrorMessage(ENTER_ADDRESS)
            return false
        }
        if(pincode.trim().isEmpty)
        {
            showErrorMessage(ENTER_PIN_CODE)
            return false
        }
        if(pincode.characters.count<6)
        {
            showErrorMessage(ENTER_VALID_PIN_CODE)
            return false
        }
        if(landMark.trim().isEmpty)
        {
            showErrorMessage(ENTER_LANDMARK)
            return false
        }
        if(!veg && !nonVeg)
        {
            showErrorMessage(ENTER_PREFERENCE)
            return false
        }
        if(lat.characters.isEmpty && long.isEmpty)
        {
            showErrorMessage(ENTER_ADDRESS_FROM_MAP)
            return false
        }
        if(!isFromConfirmYourAddressScreen)
        {
            showErrorMessage(ENTER_ADDRESS_FROM_MAP)
            return false
        }
        return true
    }
    
    /**
     This method Show the Error Message
     */
    class func showErrorMessage(message:String)
    {
        let vc : UIViewController = HMLUtlities.getTopViewController()
        vc.showErrorMessage(message)
    }
   
    /**
     This Method Validate the ContactNumber
     */
    class func validateContactNumber(contact:NSString) -> Bool
    {
        
        if(contact.length == 10)
        {
            return true
        }
        
        return false
        
    }
   
    /**
     This method Validate the ChangePAssword
     */
    class func validateChangePassword(oldPassword:String, newPassword:String) -> Bool
    {
        
        if(oldPassword.trim().characters.count == 0)
        {
            showErrorMessage(ENTER_OLD_PASSWORD)
            return false
        }
        if(newPassword.trim().characters.count == 0)
        {
            showErrorMessage(ENTER_NEW_PASSWORD)
            return false
        }
        
        return true
        
    }
    
    /**
     This method Validate the Email
     */
    class func isValidEmail(email:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(email)
    }
}

